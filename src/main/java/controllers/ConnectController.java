/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.UserKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import service.ClientService;
import service.ConseillerService;

/**
 * Controller used to connect and display connection page for both member and
 * advisor
 * @author edarles
 */
@Controller
public class ConnectController extends ProtectedController {

    //==========================================================================
    //ATTRIBUTES
    @Autowired
    private ClientService clientService;
    
    @Autowired
    private ConseillerService conseillerService;
    
    private static String invalidConn = "<div class=\"card-panel red lighten-1\">\n"
                    + "<span class=\"white-text\">Numéro client ou Mot de passe invalide</span>\n"
                    + "<i class=\"material-icons right white-text\">warning</i>\n"
                    + "</div>";
    
    private static String invalidPro = "<div class=\"card-panel red lighten-1\">\n"
                    + "<span class=\"white-text\">Nom d'utilisateur ou Mot de passe invalide</span>\n"
                    + "<i class=\"material-icons right white-text\">warning</i>\n"
                    + "</div>";

    //==========================================================================
    //METHODS

    /**
     * Display de project index page
     * 
     * @return
     */
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String initIndex() {
        return "welcomePage";
    }

    /**
     * Display member connection page
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "memberSpace", method = RequestMethod.GET)
    public String initMemberSpace(HttpServletRequest request) {
        UserKind u = CheckConnection(request);

        if (null == u) {
            return "connect/memberSpacePage";
        } else switch (u) {
            case MEMBER:
                return "redirect:/memberDashboard.htm";
            default:
                return "connect/memberSpacePage";
        }
    }

    /**
     * Display pro connection page
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "proSpace", method = RequestMethod.GET)
    public String initProSpace(HttpServletRequest request) {
        UserKind u = CheckConnection(request);

        if (null == u) {
            return "connect/proSpacePage";
        } else switch (u) {
            case PRO:
                return "redirect:/advisorDashboard.htm";
            default:
                return "connect/proSpacePage";
        }
    }

    /**
     * Handle connection member
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "memberSpace", method = RequestMethod.POST)
    protected ModelAndView handleMemberConnection(HttpServletRequest request, HttpServletResponse response) {
        String login = request.getParameter("clientNumber");
        String mdp = request.getParameter("passwd");
        ModelAndView mv;
        if (clientService.TryConnectMember(login, mdp)) {
            KeepConnection(request, login, UserKind.MEMBER);
            mv = new ModelAndView("redirect:/memberDashboard.htm");
        } else {
            
            mv = new ModelAndView("connect/memberSpacePage");
            mv.addObject("errorMessage", invalidConn);
        }
        return mv;
    }

    /**
     * Handle connection advisor
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "proSpace", method = RequestMethod.POST)
    protected ModelAndView handleAdvisorConnection(HttpServletRequest request, HttpServletResponse response) {
        String login = request.getParameter("username");
        String mdp = request.getParameter("passwd");
        ModelAndView mv;
        if (conseillerService.TryConnectAdvisor(login, mdp)) {
            KeepConnection(request, login, UserKind.PRO);
            mv = new ModelAndView("redirect:/advisorDashboard.htm");
        } else {
            
            mv = new ModelAndView("connect/proSpacePage");
            mv.addObject("errorMessage", invalidPro);
        }
        return mv;
    }

    /**
     * Disconnect
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "disconnect", method = RequestMethod.GET)
    protected String discGet(HttpServletRequest request, HttpServletResponse response) {
        Disconnect(request);
        return "welcomePage";
    }

    /**
     * Disconnect
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "disconnect", method = RequestMethod.POST)
    protected String discPost(HttpServletRequest request, HttpServletResponse response) {
        Disconnect(request);
        return "welcomePage";
    }

    //=====================================================
}
