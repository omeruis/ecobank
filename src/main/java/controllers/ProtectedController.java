/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import models.UserKind;

/**
 * Controller to handle connection verification
 * @author Lavenant
 */
public class ProtectedController {
    
    //==========================================================================
    //METHODS

    /**
     * Check if a user is currently connected and if it's a member or an advisor
     * 
     * @param request
     * @return
     */
    public UserKind CheckConnection(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") != null) {
            Object o = session.getAttribute("userKind");
            if (o instanceof UserKind) {
                return (UserKind) o;
            }
        }
        return null;
    }

    /**
     * Insert connection data into the session
     * 
     * @param request
     * @param login
     * @param uk
     */
    public void KeepConnection(HttpServletRequest request, String login, UserKind uk) {
        HttpSession session = request.getSession();
        session.setAttribute("user", login);
        session.setAttribute("userKind", uk);
    }

    /**
     * Remode connection datas from the session
     * 
     * @param request
     */
    public void Disconnect(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
        session.removeAttribute("userKind");
    }
}
