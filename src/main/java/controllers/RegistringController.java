/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Gender;
import models.MemberSubscribeForm;
import models.ProSubscribeForm;
import models.SubscribeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import service.SubscribeService;

/**
 * Handle all the regristration logic
 * @author omer
 */
@Controller
public class RegistringController {

    //==========================================================================
    //ATTRIBUTES
    
    @Autowired
    private SubscribeService subscribeService;

    private static String valid = "<div class=\"card-panel green lighten-1\">\n"
            + "<span class=\"white-text\">Compte crée avec succès, votre numéro de client vous a été envoyé par mail</span>\n"
            + "<i class=\"material-icons right white-text\">check</i>\n"
            + "</div>";
    private static String validPro = "<div class=\"card-panel green lighten-1\">\n"
            + "<span class=\"white-text\">Compte crée avec succès</span>\n"
            + "<i class=\"material-icons right white-text\">check</i>\n"
            + "</div>";
    private static String error = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Une erreur s'est produite, veuillez réessayer</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    private static String emptyFields = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Un ou plusieurs champs sont vides</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    private static String passInv = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Le mot de passe et sa confirmation ne correspondent pas</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";

    //==========================================================================
    //METHODS
    
    /**
     * Formulaire d'enregistrement des informations personnelles
     *
     * @return
     */
    @RequestMapping(value = "subscribeMember", method = RequestMethod.GET)
    public String initIndexMember() {
        return "subscribe/memberSubscribe";
    }

    /**
     * Formulaire d'enregistrement des informations personnelles
     *
     * @return
     */
    @RequestMapping(value = "subscribePro", method = RequestMethod.GET)
    public String initIndexPro() {
        return "subscribe/proSubscribe";
    }

    /**
     * ENregistrement du formulaire membre
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "subscribeMember", method = RequestMethod.POST)
    protected ModelAndView handleFormMember(HttpServletRequest request, HttpServletResponse response) {
        MemberSubscribeForm formData = new MemberSubscribeForm();

        FillSubscribeForm(formData, request);

        ModelAndView mv;
        if (subscribeService.MemberFormPasswordEquals(formData)) {
            if (subscribeService.MemberFormWellFilled(formData)) {
                if (subscribeService.SubscribeMember(formData)) {
                    mv = new ModelAndView("connect/memberSpacePage");
                    mv.addObject("errorMessage", valid);
                } else {
                    mv = new ModelAndView("subscribe/memberSubscribe");
                    mv.addObject("errorMessage", error);
                }
            } else {
                mv = new ModelAndView("subscribe/memberSubscribe");
                mv.addObject("errorMessage", emptyFields);
            }
        } else {
            mv = new ModelAndView("subscribe/memberSubscribe");
            mv.addObject("errorMessage", passInv);
        }
        return mv;
    }

    /**
     * Save the pro subscribe form
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "subscribePro", method = RequestMethod.POST)
    protected ModelAndView handleFormPro(HttpServletRequest request, HttpServletResponse response) {
        ProSubscribeForm formData = new ProSubscribeForm();

        FillSubscribeForm(formData, request);

        ModelAndView mv;
        if (subscribeService.ProFormPasswordEquals(formData)) {
            if (subscribeService.ProFormWellFilled(formData)) {
                if (subscribeService.SubscribePro(formData)) {
                    mv = new ModelAndView("connect/proSpacePage");
                    mv.addObject("errorMessage", validPro);
                } else {
                    mv = new ModelAndView("subscribe/proSubscribe");
                    mv.addObject("errorMessage", error);
                }
            } else {
                mv = new ModelAndView("subscribe/proSubscribe");
                mv.addObject("errorMessage", emptyFields);
            }
        } else {
            mv = new ModelAndView("subscribe/proSubscribe");
            mv.addObject("errorMessage", passInv);
        }
        return mv;
    }

    //==========================================================================
    //HELPERS
    
    public static void FillSubscribeForm(SubscribeForm formData, HttpServletRequest request) {
        formData.setLast_name(request.getParameter("last_name"));
        formData.setFirst_name(request.getParameter("first_name"));
        formData.setGender((request.getParameter("gender") == null) ? Gender.MALE : Gender.FEMALE);
        formData.setPasswd(request.getParameter("passwd"));
        formData.setPasswdConfirm(request.getParameter("passwdConfirm"));
        formData.setEmail(request.getParameter("email"));
        formData.setPhone(request.getParameter("phone"));
        formData.setAdress(request.getParameter("adress"));
        formData.setPostal_code(request.getParameter("postal_code"));
        formData.setCity(request.getParameter("city"));
        formData.setCountry(request.getParameter("country"));
    }
}
