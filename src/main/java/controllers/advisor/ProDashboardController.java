/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.advisor;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Displays the advisor dashboard
 * @author tlavenan
 */
@Controller
public class ProDashboardController {
    @RequestMapping(value = "advisorDashboard", method = RequestMethod.GET)
    public String dashboardOpenGet() {
        return "dashboard/advisor/advisorDashboardPage";
    }
    
    @RequestMapping(value = "advisorDashboard", method = RequestMethod.POST)
    public String dashboardOpenPost() {
        return "dashboard/advisor/advisorDashboardPage";
    }
}
