/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.advisor.calendar;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Not yet implemented, displays the advisor calendar
 * @author tlavenan
 */
@Controller
public class AdvisorCalendarController {
    @RequestMapping(value = "advisorCalendar", method = RequestMethod.GET)
    public String dashboardOpenGet() {
        return "dashboard/advisor/calendar/advisorCalendarPage";
    }
    
    @RequestMapping(value = "advisorCalendar", method = RequestMethod.POST)
    public String dashboardOpenPost() {
        return "dashboard/advisor/calendar/advisorCalendarPage";
    }
}
