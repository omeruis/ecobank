/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.advisor.clients;

import controllers.ProtectedController;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import models.AccountCard;
import models.AccountDetail;
import models.Client;
import models.NavigationContainer;
import models.UserKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import service.AccountService;
import service.ConseillerService;
import service.NavigationService;

/**
 * Handle all the logic of the 1..N relationship between client and advisor
 * @author tlavenan
 */
@Controller
public class AdvisorClientsController extends ProtectedController {

    //==========================================================================
    //ATTRIBUTES
    
    static int maxPageNum = 9;
    @Autowired
    private AccountService accountService;
    @Autowired
    private NavigationService navigationService;
    @Autowired
    private ConseillerService conseillerService;

    //==========================================================================
    //METHODS

    /**
     * Displays the list of the clients for the connected advisor
     * @param request
     * @return
     */
    @RequestMapping(value = "advisorClients", method = RequestMethod.GET)
    public ModelAndView dashboardOpen(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/proSpace.htm");
            return mv;
        } else {
            switch (u) {
                case PRO:
                    mv = new ModelAndView("dashboard/advisor/clients/advisorClientsPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/proSpace.htm");
                    return mv;
            }
        }
        NavigationContainer nav = navigationService.ComputeNavigation(conseillerService.getConseillerClientsPageNumber(), request.getParameter("page"));

        List<Client> clients = conseillerService.getConseillerClients(nav.currentPage);
        mv.addObject("clients", clients);
        mv.addObject("currentPage", nav.currentPage);
        mv.addObject("firstPage", nav.firstPage);
        mv.addObject("lastPage", nav.lastPage);
        return mv;
    }

    /**
     * Show a particular client detail.
     * @param request
     * @return
     */
    @RequestMapping(value = "advisorShowClient", method = RequestMethod.GET)
    public ModelAndView clientDetail(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/proSpace.htm");
            return mv;
        } else {
            switch (u) {
                case PRO:
                    mv = new ModelAndView("dashboard/advisor/clients/advisorClientDetailPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/proSpace.htm");
                    return mv;
            }
        }

        List<AccountCard> accounts = accountService.GetMemberAccounts(request.getParameter("id"));
        mv.addObject("accounts", accounts);

        return mv;
    }

    /**
     * Show an account from a client of the current advisor
     * @param request
     * @return
     */
    @RequestMapping(value = "advisorShowClientAccount", method = RequestMethod.GET)
    public ModelAndView AccountDetailGet(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/proSpace.htm");
            return mv;
        } else {
            switch (u) {
                case PRO:
                    mv = new ModelAndView("dashboard/advisor/clients/advisorClientAccountDetailPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/proSpace.htm");
                    return mv;
            }
        }

        String id = request.getParameter("id");

        if (id == null) {
            mv = new ModelAndView("redirect:/advisorClients.htm");
            return mv;
        }

        NavigationContainer nav = navigationService.ComputeNavigation(accountService.GetAccountDetailPageNumber(id), request.getParameter("page"));

        AccountDetail detail = accountService.GetAccountDetail(request.getParameter("id"), nav.currentPage);
        mv.addObject("currentPage", nav.currentPage);
        mv.addObject("firstPage", nav.firstPage);
        mv.addObject("lastPage", nav.lastPage);
        mv.addObject("detail", detail);

        return mv;
    }
}
