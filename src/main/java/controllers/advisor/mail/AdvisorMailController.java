/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.advisor.mail;

import controllers.ProtectedController;
import controllers.member.accounts.AccountDetailController;
import models.NavigationContainer;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import models.Client;
import models.Mail;
import models.UserKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import service.ConseillerService;
import service.EmailService;
import service.NavigationService;

/**
 * Handle all the mailing logic
 * @author tlavenan
 */
@Controller
public class AdvisorMailController extends ProtectedController {
    //==========================================================================
    //ATTRIBUTES
    
    static String sendSuccess = "<div class=\"card-panel green lighten-1\">\n"
            + "<span class=\"white-text\">Message envoyé avec succès</span>\n"
            + "<i class=\"material-icons right white-text\">check</i>\n"
            + "</div>";
    static String sendFailure = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Erreur lors de l'envoi du message</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    
    @Autowired
    private EmailService emailService;
    @Autowired
    private ConseillerService conseillerService;
    @Autowired
    private NavigationService navigationService;
    
    static int maxPageNum = 9;

    //==========================================================================
    //METHODS

    /**
     * Display the advisor mails
     * @param request
     * @return
     * @throws UnsupportedEncodingException
     */
    @RequestMapping(value = "advisorMail", method = RequestMethod.GET)
    public ModelAndView AccountDetailGet(HttpServletRequest request) throws UnsupportedEncodingException {
        UserKind u = CheckConnection(request);
        ModelAndView mv;
        
        if (null == u) {
            mv = new ModelAndView("redirect:/proSpace.htm");
            return mv;
        } else {
            switch (u) {
                case PRO:
                    mv = new ModelAndView("dashboard/advisor/mail/advisorMailPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/proSpace.htm");
                    return mv;
            }
        }
        
        NavigationContainer nav = navigationService.ComputeNavigation(emailService.getInternalEmailPageNum(), request.getParameter("page"));
        
        List<Mail> messages = emailService.getInternalEmailPage(nav.currentPage, UserKind.PRO);
        mv.addObject("currentPage", nav.currentPage);
        mv.addObject("firstPage", nav.firstPage);
        mv.addObject("lastPage", nav.lastPage);
        mv.addObject("messages", messages);
        if (request.getParameter("message") != null) {
            mv.addObject("message", URLDecoder.decode(request.getParameter("message"), "UTF-8"));
        }
        List<Client> clients = conseillerService.getConseillerClients(0);
        mv.addObject("clients", clients);
        return mv;
    }
    
    /**
     * Send a mail to a client
     * @param request
     * @return
     */
    @RequestMapping(value = "sendMessageClient", method = RequestMethod.POST)
    public ModelAndView SendMessageToAdvisor(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;
        
        if (null == u) {
            mv = new ModelAndView("redirect:/proSpace.htm");
            return mv;
        } else {
            switch (u) {
                case PRO:
                    mv = new ModelAndView("redirect:/advisorMail.htm");
                    break;
                default:
                    mv = new ModelAndView("redirect:/proSpace.htm");
                    return mv;
            }
        }
        
        String subject = request.getParameter("subject");
        String bodyMessage = request.getParameter("bodyMail");
        String clientId = request.getParameter("clientNum");
        
        try {
            if (emailService.sendInternalEmail(clientId, subject, bodyMessage)) {
                mv.addObject("message", URLEncoder.encode(sendSuccess, "UTF-8"));
            } else {
                mv.addObject("message", URLEncoder.encode(sendFailure, "UTF-8"));
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AccountDetailController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return mv;
    }
}
