/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.advisor.notifications;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Not implemented, displays the advisor notifications
 * @author tlavenan
 */
@Controller
public class AdvisorNotificationsController {
    @RequestMapping(value = "advisorNotifications", method = RequestMethod.GET)
    public String dashboardOpenGet() {
        return "dashboard/advisor/notifications/advisorNotificationsPage";
    }
    
    @RequestMapping(value = "advisorNotifications", method = RequestMethod.POST)
    public String dashboardOpenPost() {
        return "dashboard/advisor/notifications/advisorNotificationsPage";
    }
}
