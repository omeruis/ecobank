/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.advisor.personalInformations;

import controllers.ProtectedController;
import controllers.RegistringController;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.MemberSubscribeForm;
import models.ProSubscribeForm;
import models.UserKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import service.SubscribeService;

/**
 * Handle the advisor personal informations logic
 * @author tlavenan
 */
@Controller
public class AdvisorPersonalInformationsController extends ProtectedController {
    
    //==========================================================================
    //ATTRIBUTES
    
    @Autowired
    private SubscribeService subscribeService;

    private static String valid = "<div class=\"card-panel green lighten-1\">\n"
            + "<span class=\"white-text\">Informations mises à jour avec succès</span>\n"
            + "<i class=\"material-icons right white-text\">check</i>\n"
            + "</div>";
    private static String error = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Une erreur s'est produite, veuillez réessayer</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    private static String emptyFields = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Un ou plusieurs champs sont vides</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    private static String passInv = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Le mot de passe et sa confirmation ne correspondent pas</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";

    //==========================================================================
    //METHODS

    /**
     * Displays the advisor personal informations. Prefill
     * @param request
     * @return
     * @throws UnsupportedEncodingException
     */
    @RequestMapping(value = "advisorPersonalInformation", method = RequestMethod.GET)
    public ModelAndView infoGet(HttpServletRequest request) throws UnsupportedEncodingException {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/proSpace.htm");
            return mv;
        } else {
            switch (u) {
                case PRO:
                    mv = new ModelAndView("dashboard/advisor/persoInformations/advisorPersonalInformationPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/proSpace.htm");
                    return mv;
            }
        }
        
        MemberSubscribeForm formData = subscribeService.GetUserData(request.getSession().getAttribute("user"));
        mv.addObject("prefill", formData);
        if (request.getParameter("message") != null) {
            mv.addObject("message", URLDecoder.decode(request.getParameter("message"), "UTF-8"));
        }
        
        return mv;
    }

    /**
     * Update the advisor personal informations
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "advisorPersonalInformationUpdate", method = RequestMethod.POST)
    protected ModelAndView handleFormMember(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/proSpace.htm");
            return mv;
        } else {
            switch (u) {
                case PRO:
                    mv = new ModelAndView("redirect:/advisorPersonalInformation.htm");
                    break;
                default:
                    mv = new ModelAndView("redirect:/proSpace.htm");
                    return mv;
            }
        }
        ProSubscribeForm formData = new ProSubscribeForm();

        RegistringController.FillSubscribeForm(formData, request);

        if (subscribeService.ProFormPasswordEquals(formData)) {
            if (subscribeService.ProFormWellFilled(formData)) {
                if (subscribeService.UpdateAdvisor(formData)) {
                    mv.addObject("message", URLEncoder.encode(valid, "UTF-8"));
                } else {
                    mv.addObject("message", URLEncoder.encode(error, "UTF-8"));
                }
            } else {
                mv.addObject("message", URLEncoder.encode(emptyFields, "UTF-8"));
            }
        } else {
            mv.addObject("message", URLEncoder.encode(passInv, "UTF-8"));
        }
        mv.addObject("prefill", formData);
        return mv;
    }
}
