/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.member;

import controllers.ProtectedController;
import javax.servlet.http.HttpServletRequest;
import models.UserKind;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author tlavenan
 */
@Controller
public class MemberDashboradController extends ProtectedController {
    @RequestMapping(value = "memberDashboard", method = RequestMethod.GET)
    public String dashboardOpenGet(HttpServletRequest request) {
        UserKind u = CheckConnection(request);

        if (null == u) {
            return "redirect:/memberSpace.htm";
        } else switch (u) {
            case MEMBER:
                return "dashboard/member/memberDashboardPage";
            default:
                return "redirect:/memberSpace.htm";
        }
    }
}
