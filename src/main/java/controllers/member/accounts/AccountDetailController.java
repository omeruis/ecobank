/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.member.accounts;

import models.NavigationContainer;
import controllers.ProtectedController;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import models.AccountCard;
import models.AccountDetail;
import models.AccountType;
import models.Beneficiaire;
import models.UserKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import service.AccountService;
import service.NavigationService;

/**
 * Handle client account logic
 * @author tlavenan
 */
@Controller
public class AccountDetailController extends ProtectedController {

    //==========================================================================
    //ATTRIBUTES
    
    @Autowired
    private AccountService accountService;
    @Autowired
    private NavigationService navigationService;
    
    static String accountCreationSuccess = "<div class=\"card-panel green lighten-1\">\n"
            + "<span class=\"white-text\">Compte créé avec succès</span>\n"
            + "<i class=\"material-icons right white-text\">check</i>\n"
            + "</div>";
    static String accountCreationFailure = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Erreur lors de la création de compte</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    static String transactionSuccess = "<div class=\"card-panel green lighten-1\">\n"
            + "<span class=\"white-text\">Virement effectué avec succès</span>\n"
            + "<i class=\"material-icons right white-text\">check</i>\n"
            + "</div>";
    static String transactionFailure = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Erreur lors du virement. Vous ne serez pas débité</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    static String ajoutBenefSuccess = "<div class=\"card-panel green lighten-1\">\n"
            + "<span class=\"white-text\">Bénéficiaire ajouté avec succès</span>\n"
            + "<i class=\"material-icons right white-text\">check</i>\n"
            + "</div>";
    static String ajoutBenefFailure = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Erreur lors de la création du bénéficiaire</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    static int maxPageNum = 9;

    //==========================================================================
    //METHODS

    /**
     * Displays the member accounts
     * 
     * @param request
     * @return
     * @throws UnsupportedEncodingException
     */
    @RequestMapping(value = "memberAccounts", method = RequestMethod.GET)
    public ModelAndView accountsGet(HttpServletRequest request) throws UnsupportedEncodingException {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("dashboard/member/accounts/memberAccountsPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }

        List<AccountCard> accounts = accountService.GetMemberAccounts((String) request.getSession().getAttribute("user"));
        mv.addObject("accounts", accounts);
        List<Beneficiaire> beneficiaires = accountService.GetMemberBeneficiaires();
        mv.addObject("beneficiaires", beneficiaires);
        if (request.getParameter("message") != null) {
            mv.addObject("message", URLDecoder.decode(request.getParameter("message"), "UTF-8"));
        }

        return mv;
    }

    /**
     * Allow a user to create an account
     * @param request
     * @return
     */
    @RequestMapping(value = "createAccount", method = RequestMethod.POST)
    public ModelAndView accountCreate(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("dashboard/member/accounts/memberAccountsPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }

        String type = request.getParameter("accountType");
        boolean b = false;
        if (type != null) {
            if (type.equals("CURENT")) {
                b = accountService.CreateAccount(AccountType.CURENT);
            } else if (type.equals("SAVE")) {
                b = accountService.CreateAccount(AccountType.SAVE);
            } else if (type.equals("STOCK")) {
                b = accountService.CreateAccount(AccountType.STOCK);
            }
        }

        if (b) {
            mv.addObject("message", accountCreationSuccess);
        } else {
            mv.addObject("message", accountCreationFailure);
        }
        List<AccountCard> accounts = accountService.GetMemberAccounts((String) request.getSession().getAttribute("user"));
        mv.addObject("accounts", accounts);
        return mv;
    }

    /**
     * Allow someone to get the account details
     * @param request
     * @return
     */
    @RequestMapping(value = "account", method = RequestMethod.GET)
    public ModelAndView AccountDetailGet(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("dashboard/member/accounts/memberAccountDetailPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }

        String id = request.getParameter("id");

        if (id == null) {
            mv = new ModelAndView("redirect:/memberAccounts.htm");
            return mv;
        }

        NavigationContainer nav = navigationService.ComputeNavigation(accountService.GetAccountDetailPageNumber(request.getParameter("id")), request.getParameter("page"));

        AccountDetail detail = accountService.GetAccountDetail(request.getParameter("id"), nav.currentPage);
        mv.addObject("currentPage", nav.currentPage);
        mv.addObject("firstPage", nav.firstPage);
        mv.addObject("lastPage", nav.lastPage);
        mv.addObject("detail", detail);
        List<Beneficiaire> beneficiaires = accountService.GetMemberBeneficiaires();
        mv.addObject("beneficiaires", beneficiaires);

        return mv;
    }

    /**
     * Alow one to create a transaction (virements)
     * @param request
     * @return
     */
    @RequestMapping(value = "requestTransaction", method = RequestMethod.POST)
    public ModelAndView RequestTransaction(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("redirect:/memberAccounts.htm");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }

        String emeteur = request.getParameter("accountNum");
        String beneficiaire = request.getParameter("benefNum");
        double amount = Integer.parseInt(request.getParameter("amount"));
        String libelle = request.getParameter("libelle");
        String detail = request.getParameter("detail");

        try {
            if (accountService.CreateTransaction(beneficiaire, emeteur, amount, libelle, detail)) {
                mv.addObject("message", URLEncoder.encode(transactionSuccess, "UTF-8"));
            } else {
                mv.addObject("message", URLEncoder.encode(transactionFailure, "UTF-8"));
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AccountDetailController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return mv;
    }

    /**
     * Add someone to send a transaction to
     * @param request
     * @return
     */
    @RequestMapping(value = "addBeneficiaire", method = RequestMethod.POST)
    public ModelAndView addBeneficiaire(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("redirect:/memberAccounts.htm");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }

        String benefName = request.getParameter("benefName");
        String benefAccNum = request.getParameter("benefAccNum");
        String benefIban = request.getParameter("benefIban");

        try {
            if (accountService.CreateBeneficiaire(benefName, benefAccNum, benefIban)) {
                mv.addObject("message", URLEncoder.encode(ajoutBenefSuccess, "UTF-8"));
            } else {
                mv.addObject("message", URLEncoder.encode(ajoutBenefFailure, "UTF-8"));
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AccountDetailController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return mv;
    }
}
