/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.member.calendal;

import controllers.member.accounts.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author tlavenan
 */
@Controller
public class CalendarController {
    @RequestMapping(value = "calendar", method = RequestMethod.GET)
    public String AccountDetailGet() {
        return "dashboard/member/calendar/memberCalendarPage";
    }
    
    @RequestMapping(value = "calendar", method = RequestMethod.POST)
    public String AccountDetailPost() {
        return "error/error";
    }
}
