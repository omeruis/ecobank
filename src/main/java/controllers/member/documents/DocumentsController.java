/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.member.documents;

import controllers.ProtectedController;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import models.AccountCard;
import models.UserKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import service.AccountService;
import service.DocumentService;

/**
 *
 * @author tlavenan
 */
@Controller
public class DocumentsController extends ProtectedController {
    
    @Autowired
    private AccountService accountService;
    
    @Autowired
    private DocumentService documentService;
    
    static String success = "<div class=\"card-panel green lighten-1\">\n"
            + "<span class=\"white-text\">Votre demande à bien été prise en compte</span>\n"
            + "<i class=\"material-icons right white-text\">check</i>\n"
            + "</div>";
    
    static String failure = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Erreur lors de l'enregistrement de la demande</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    
    
    @RequestMapping(value = "documents", method = RequestMethod.GET)
    public ModelAndView HandleDocuments(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("dashboard/member/documents/memberDocumentsPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }

        List<AccountCard> accounts = accountService.GetMemberAccounts((String) request.getSession().getAttribute("user"));
        mv.addObject("accounts", accounts);
        return mv;
    }
    
    @RequestMapping(value = "askCard", method = RequestMethod.POST)
    public ModelAndView HandleAskCard(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("dashboard/member/documents/memberDocumentsPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }

        String accountNumber = request.getParameter("accountNum");
        if (accountNumber == null || !documentService.CreateAskCard(accountNumber)){
            mv.addObject("errorMessage", failure);
        } else {
            mv.addObject("errorMessage", success);
        }
        List<AccountCard> accounts = accountService.GetMemberAccounts((String) request.getSession().getAttribute("user"));
        mv.addObject("accounts", accounts);
        return mv;
    }
    
    @RequestMapping(value = "askCheck", method = RequestMethod.POST)
    public ModelAndView HandleAskCheck(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("dashboard/member/documents/memberDocumentsPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }
        
        String accountNumber = request.getParameter("accountNum");
        if (accountNumber == null || !documentService.CreateAskCheck(accountNumber)){
            mv.addObject("errorMessage", failure);
        } else {
            mv.addObject("errorMessage", success);
        }
        List<AccountCard> accounts = accountService.GetMemberAccounts((String) request.getSession().getAttribute("user"));
        mv.addObject("accounts", accounts);
        return mv;
    }
}
