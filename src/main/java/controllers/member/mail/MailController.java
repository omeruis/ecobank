/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.member.mail;

import models.NavigationContainer;
import controllers.ProtectedController;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import models.Mail;
import models.UserKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import service.EmailService;
import service.NavigationService;

/**
 * Handles communication with the advisor
 * @author tlavenan
 */
@Controller
public class MailController extends ProtectedController {

    //==========================================================================
    //ATTRIBUTES
    
    @Autowired
    private EmailService emailService;
    @Autowired
    private NavigationService navigationService;
    
    static int maxPageNum = 9;

    //==========================================================================
    //METHODS

    /**
     * Displays the member mailing box
     * @param request
     * @return
     */
    @RequestMapping(value = "messagerieMembre", method = RequestMethod.GET)
    public ModelAndView AccountDetailGet(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;
        
        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("dashboard/member/mail/memberMailPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }
        
        NavigationContainer nav = navigationService.ComputeNavigation(emailService.getInternalEmailPageNum(), request.getParameter("page"));
        
        List<Mail> messages = emailService.getInternalEmailPage(nav.currentPage, UserKind.MEMBER);
        mv.addObject("currentPage", nav.currentPage);
        mv.addObject("firstPage", nav.firstPage);
        mv.addObject("lastPage", nav.lastPage);
        mv.addObject("messages", messages);
        
        return mv;
    }
    
    /**
     * Sends a message to an advisor
     * @param request
     * @return
     */
    @RequestMapping(value = "sendMessageAdvisor", method = RequestMethod.POST)
    public ModelAndView SendMessageToAdvisor(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;
        
        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("redirect:/messagerieMembre.htm");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }
        
        String subject = request.getParameter("subject");
        String bodyMessage = request.getParameter("bodyMail");
        
        emailService.sendInternalEmail(subject, bodyMessage);
        
        return mv;
    }
}
