/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.member.notifications;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Not implemented, handle all the notification logic
 * @author tlavenan
 */
@Controller
public class NotificationController {
    @RequestMapping(value = "notifications", method = RequestMethod.GET)
    public String AccountDetailGet() {
        return "dashboard/member/notificationCenter/memberNotificationCenterPage";
    }
    
    @RequestMapping(value = "notifications", method = RequestMethod.POST)
    public String AccountDetailPost() {
        return "error/error";
    }
}
