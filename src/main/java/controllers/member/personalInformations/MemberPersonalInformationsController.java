/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.member.personalInformations;

import controllers.ProtectedController;
import controllers.RegistringController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.MemberSubscribeForm;
import models.UserKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import service.SubscribeService;

/**
 * Handle member personal informations
 * @author tlavenan
 */
@Controller
public class MemberPersonalInformationsController extends ProtectedController {

    //==========================================================================
    //ATTRIBUTES
    
    @Autowired
    private SubscribeService subscribeService;

    private static String valid = "<div class=\"card-panel green lighten-1\">\n"
            + "<span class=\"white-text\">Informations mises à jour avec succès</span>\n"
            + "<i class=\"material-icons right white-text\">check</i>\n"
            + "</div>";
    private static String error = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Une erreur s'est produite, veuillez réessayer</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    private static String emptyFields = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Un ou plusieurs champs sont vides</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";
    private static String passInv = "<div class=\"card-panel red lighten-1\">\n"
            + "<span class=\"white-text\">Le mot de passe et sa confirmation ne correspondent pas</span>\n"
            + "<i class=\"material-icons right white-text\">warning</i>\n"
            + "</div>";

    //==========================================================================
    //METHODS

    /**
     * Displays the member personal informations. Prefilled
     * @param request
     * @return
     */
    @RequestMapping(value = "memberPersonalInformation", method = RequestMethod.GET)
    public ModelAndView infoGet(HttpServletRequest request) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("dashboard/member/persoInformations/memberPersonalInformationPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }
        
        MemberSubscribeForm formData = subscribeService.GetUserData(request.getSession().getAttribute("user"));
        mv.addObject("prefill", formData);
        
        return mv;
    }

    /**
     * Allow personal information update
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "memberPersonalInformationUpdate", method = RequestMethod.POST)
    protected ModelAndView handleFormMember(HttpServletRequest request, HttpServletResponse response) {
        UserKind u = CheckConnection(request);
        ModelAndView mv;

        if (null == u) {
            mv = new ModelAndView("redirect:/memberSpace.htm");
            return mv;
        } else {
            switch (u) {
                case MEMBER:
                    mv = new ModelAndView("dashboard/member/persoInformations/memberPersonalInformationPage");
                    break;
                default:
                    mv = new ModelAndView("redirect:/memberSpace.htm");
                    return mv;
            }
        }
        MemberSubscribeForm formData = new MemberSubscribeForm();

        RegistringController.FillSubscribeForm(formData, request);

        if (subscribeService.MemberFormPasswordEquals(formData)) {
            if (subscribeService.MemberFormWellFilled(formData)) {
                if (subscribeService.UpdateMember(formData)) {
                    mv.addObject("errorMessage", valid);
                } else {
                    mv.addObject("errorMessage", error);
                }
            } else {
                mv.addObject("errorMessage", emptyFields);
            }
        } else {
            mv.addObject("errorMessage", passInv);
        }
        mv.addObject("prefill", formData);
        return mv;
    }
}
