/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Lavenant
 */
public class AccountCard {
    private AccountType Type;
    private String Number;
    private Double Amount;
    private AccountHealth Health;

    /**
     * @return the Type
     */
    public AccountType getType() {
        return Type;
    }

    /**
     * @param Type the Type to set
     */
    public void setType(AccountType Type) {
        this.Type = Type;
    }

    /**
     * @return the Number
     */
    public String getNumber() {
        return Number;
    }

    /**
     * @param Number the Number to set
     */
    public void setNumber(String Number) {
        this.Number = Number;
    }

    /**
     * @return the Amount
     */
    public Double getAmount() {
        return Amount;
    }

    /**
     * @param Amount the Amount to set
     */
    public void setAmount(Double Amount) {
        this.Amount = Amount;
    }

    /**
     * @return the Health
     */
    public AccountHealth getHealth() {
        return Health;
    }

    /**
     * @param Health the Health to set
     */
    public void setHealth(AccountHealth Health) {
        this.Health = Health;
    }
}
