/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Lavenant
 */
public class Client {
    private String name;
    private String prenom;
    private String id;
    private double soldeGlobal;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the soldeGlobal
     */
    public double getSoldeGlobal() {
        return soldeGlobal;
    }

    /**
     * @param soldeGlobal the soldeGlobal to set
     */
    public void setSoldeGlobal(double soldeGlobal) {
        this.soldeGlobal = soldeGlobal;
    }
}
