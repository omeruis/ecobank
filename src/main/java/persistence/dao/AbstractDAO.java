/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.transaction.annotation.Transactional;

/**
 * Cette classe nous evite d'avoir a redefinir les mêmes methodes pour chaque DAO
 * @author omer
 * @param <T> La classe à implementer
 */
public abstract class AbstractDAO<T> {
    private final Class<T> entityClass;

    /**
     * Constructeur de la classe
     * @param entityClass La classe entité
     */
    public AbstractDAO(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
    
    /**
     * Permet de recupérer l'objet EntityManager
     * @return EntityManager 
     */
    protected abstract EntityManager getEntityManager();
    
    /**
     * Permet de créer un enregistrement dans la base de données
     * @param entity 
     */
    @Transactional
    public void create(T entity) {
        getEntityManager().persist(entity);
    }
    
    /**
     * Permet de mettre a jour les données de la base de données
     * @param entity 
     */
    @Transactional
    public void update(T entity) {
        getEntityManager().merge(entity);
    }

    /**
     * Permet de supprimer un élément de la base de données
     * @param entity 
     */
    @Transactional
    public void delete(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }
    
    /**
     * Permet de rechercher un enregistrement de la base de données
     * via son id
     * @param id clé primaire
     * @return T une entité 
     */
    @Transactional(readOnly = true)
    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }
    
    /**
     * Retourne la liste de tous les enregistrements dans la table de l'entité
     * @return Liste d'objets
     */
    @Transactional(readOnly = true)
    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }
}
