/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.ActionEntity;

/**
 *
 * @author omer
 */
public interface ActionDAO {

    /**
     * Permet de créer une action dans la base de données
     * @param a 
     */
    void create(ActionEntity a);
    
    /**
     * Permet de mettre a jour une action dans la base de données
     * @param a 
     */
    void update(ActionEntity a);
    
    /**
     * Permet de supprimer une action dans la base de données
     * @param a 
     */
    void delete(ActionEntity a);
    
    /**
     * Permet de recuperer une action depuis la base de données
     * @param id
     * @return Une entité action
     */
    ActionEntity find(Object id);
    
    /**
     * Retourne la liste de toutes les actions dans la table de l'entité
     * @return 
     */
    List<ActionEntity> findAll();
}
