/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.BeneficiaireEntity;

/**
 *
 * @author omer
 */
public interface BeneficiaireDAO {
    /**
     * Permet de créer un beneficaire
     * @param b 
     */
    void create(BeneficiaireEntity b);
    
    /**
     * Permet de mettre à jour un bénéficiare
     * @param b 
     */
    void update(BeneficiaireEntity b);
    
    /**
     * Permet de supprimer un beneficiaire
     * @param b 
     */
    void delete(BeneficiaireEntity b);
    
    /**
     * Récupère un bénéficiaire via son id
     * @param id
     * @return 
     */
    BeneficiaireEntity find(Object id);
    
    /**
     * Retourne la liste de tous les bénéficiaires
     * @return 
     */
    List<BeneficiaireEntity> findAll();
    
    /**
     * Récupère un bénéficiare via son numéro de compte
     * @param numero
     * @return 
     */
    List<BeneficiaireEntity> findByNumero(String numero);
    
    /**
     * Retourne la liste des bénéficiaire d'un client
     * @param id
     * @return 
     */
    List<BeneficiaireEntity> findByClientId(Long id);
}
