/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.CarteEntity;

/**
 *
 * @author omer
 */
public interface CarteDAO {

    void create(CarteEntity c);
    void update(CarteEntity c);
    void delete(CarteEntity c);
    CarteEntity find(Object id);
    List<CarteEntity> findAll();
    CarteEntity findByNumero(String numero);
    List<CarteEntity> findByNumeroCompte(String numero);
}
