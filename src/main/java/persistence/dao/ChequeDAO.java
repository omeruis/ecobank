/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.ChequeEntity;

/**
 *
 * @author omer
 */
public interface ChequeDAO {
    void create(ChequeEntity c);
    void update(ChequeEntity c);
    void delete(ChequeEntity c);
    ChequeEntity find(Object id);
    List<ChequeEntity> findAll();
}
