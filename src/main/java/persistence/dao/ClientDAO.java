/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.ClientEntity;

/**
 *
 * @author omer
 */
public interface ClientDAO {
    void create(ClientEntity c);
    void update(ClientEntity c);
    void delete(ClientEntity c);
    ClientEntity find(Object id);
    List<ClientEntity> findAll();
    ClientEntity findByNumero(String numero);
}
