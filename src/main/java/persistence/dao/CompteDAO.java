/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.CompteEntity;

/**
 *
 * @author omer
 */
public interface CompteDAO {
    void create(CompteEntity c);
    void update(CompteEntity c);
    void delete(CompteEntity c);
    CompteEntity find(Object id);
    List<CompteEntity> findAll();
    CompteEntity findByNumero(String numero);
}
