/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.ConseillerEntity;

/**
 *
 * @author omer
 */
public interface ConseillerDAO {
    void create(ConseillerEntity c);
    void update(ConseillerEntity c);
    void delete(ConseillerEntity c);
    ConseillerEntity find(Object id);
    List<ConseillerEntity> findAll();
    ConseillerEntity findByUserName(String userName);
}
