/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.MessageBoxEntity;

/**
 *
 * @author omer
 */
public interface MessageBoxDAO {
    void create(MessageBoxEntity m);
    void update(MessageBoxEntity m);
    void delete(MessageBoxEntity m);
    MessageBoxEntity find(Object id);
    List<MessageBoxEntity> findAll();
}
