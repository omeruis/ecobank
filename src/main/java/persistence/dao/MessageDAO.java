/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.MessageEntity;

/**
 *
 * @author omer
 */
public interface MessageDAO {
    void create(MessageEntity m);
    void update(MessageEntity m);
    void delete(MessageEntity m);
    MessageEntity find(Object id);
    List<MessageEntity> findAll();
}
