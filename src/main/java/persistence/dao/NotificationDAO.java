/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.NotificationEntity;

/**
 *
 * @author omer
 */
public interface NotificationDAO {
    void create(NotificationEntity n);
    void update(NotificationEntity n);
    void delete(NotificationEntity n);
    NotificationEntity find(Object id);
    List<NotificationEntity> findAll();
}
