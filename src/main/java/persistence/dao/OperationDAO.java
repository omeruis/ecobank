/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.OperationEntity;

/**
 *
 * @author omer
 */
public interface OperationDAO {
    void create(OperationEntity o);
    void update(OperationEntity o);
    void delete(OperationEntity o);
    OperationEntity find(Object id);
    List<OperationEntity> findAll();
}
