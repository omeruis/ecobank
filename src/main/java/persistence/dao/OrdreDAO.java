/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.OrdreEntity;

/**
 *
 * @author omer
 */
public interface OrdreDAO {
    void create(OrdreEntity o);
    void update(OrdreEntity o);
    void delete(OrdreEntity o);
    OrdreEntity find(Object id);
    List<OrdreEntity> findAll();
}
