/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.dao;

import java.util.List;
import persistence.entities.VirementEntity;

/**
 *
 * @author omer
 */
public interface VirementDAO {
    void create(VirementEntity v);
    void update(VirementEntity v);
    void delete(VirementEntity v);
    VirementEntity find(Object id);
    List<VirementEntity> findAll();
    List<VirementEntity> findByNumeroBeneficiaire(String numero);
}
