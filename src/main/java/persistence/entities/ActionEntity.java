/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name = "Action")
public class ActionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "libelle")
    private String libelle;
    
    @Column(name = "valeur")
    private Double valeur;

    @OneToMany(mappedBy = "action")
    private List<OrdreEntity> ordres = new ArrayList<OrdreEntity>();

    public ActionEntity() {}

    public ActionEntity(Long id, String libelle, Double valeur) {
        this.id = id;
        this.libelle = libelle;
        this.valeur = valeur;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Double getValeur() {
        return valeur;
    }

    public void setValeur(Double valeur) {
        this.valeur = valeur;
    }

    public List<OrdreEntity> getOrdres() {
        return ordres;
    }

    public void setOrdres(List<OrdreEntity> Ordres) {
        this.ordres = Ordres;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActionEntity other = (ActionEntity) obj;
        return this.id == other.id || (this.id != null && this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "ActionEntity{" + "id=" + id + ", libelle=" + libelle + ", valeur=" + valeur + '}';
    }
}
