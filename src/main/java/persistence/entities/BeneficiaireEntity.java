/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name = "Beneficiaire", 
        uniqueConstraints = @UniqueConstraint(columnNames = {"client_id","numero_compte"}))
public class BeneficiaireEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name="client_id")
    protected ClientEntity clientBeneficiaire;
    
    @ManyToOne
    @JoinColumn(name="numero_compte")
    private CompteEntity compteCible;
    
    @Column(name = "date_ajout")
    @Temporal(TemporalType.DATE)
    private Date dateAjout;

    public BeneficiaireEntity(ClientEntity client, CompteEntity compteCible) {
        this.clientBeneficiaire = client;
        this.compteCible = compteCible;
    }

    public BeneficiaireEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientEntity getClient() {
        return clientBeneficiaire;
    }

    public void setClient(ClientEntity client) {
        this.clientBeneficiaire = client;
    }

    public CompteEntity getCompteCible() {
        return compteCible;
    }

    public void setCompteCible(CompteEntity compteCible) {
        this.compteCible = compteCible;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 41 * hash + (this.clientBeneficiaire != null ? this.clientBeneficiaire.hashCode() : 0);
        hash = 41 * hash + (this.compteCible != null ? this.compteCible.hashCode() : 0);
        hash = 41 * hash + (this.dateAjout != null ? this.dateAjout.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BeneficiaireEntity other = (BeneficiaireEntity) obj;
        return this.id == other.id || (this.id != null && this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "BeneficiaireEntity{" + "id=" + id + ", client=" + clientBeneficiaire + ", compteCible=" + compteCible + ", dateAjout=" + dateAjout + '}';
    }
       
}
