/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name = "Carte")
public class CarteEntity implements Serializable {
    
    public enum TypeCarte {
        VISA,
        CARTE_BLEU,
        MASTER_CARD
    }
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="numero_compte")
    private CompteCourantEntity compteCarte;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "type_carte")
    private TypeCarte typeCarte;
    
    @Column(nullable = false, name = "numero")
    private String numero;
    
    @Column(nullable = false, name = "cryptogramme")
    private String cryptogramme;
    
    @Column(nullable = false, name = "date_expiration")
    @Temporal(TemporalType.DATE)
    private Date dateExpiration;
    
    @Column(nullable = true, name = "frais_annuel")
    private Double fraisAnnuel;
    
    @Column(nullable = true, name = "plafond_retrait")
    private Double plafondRetrait;
    
    @Column(nullable = true, name = "plafond_paiement")
    private Double plafondPaiement;

    public CompteCourantEntity getCompteCarte() {
        return compteCarte;
    }

    public void setCompteCarte(CompteCourantEntity compteCarte) {
        this.compteCarte = compteCarte;
    }

    public TypeCarte getTypeCarte() {
        return typeCarte;
    }

    public void setTypeCarte(TypeCarte typeCarte) {
        this.typeCarte = typeCarte;
    }

    public CarteEntity() {
    }

    public CarteEntity(String numero, String cryptogramme, Date dateExpiration) {
        this.numero = numero;
        this.cryptogramme = cryptogramme;
        this.dateExpiration = dateExpiration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCryptogramme() {
        return cryptogramme;
    }

    public void setCryptogramme(String cryptogramme) {
        this.cryptogramme = cryptogramme;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public Double getFraisAnnuel() {
        return fraisAnnuel;
    }

    public void setFraisAnnuel(Double fraisAnnuel) {
        this.fraisAnnuel = fraisAnnuel;
    }

    public Double getPlafondRetrait() {
        return plafondRetrait;
    }

    public void setPlafondRetrait(Double plafondRetrait) {
        this.plafondRetrait = plafondRetrait;
    }

    public Double getPlafondPaiement() {
        return plafondPaiement;
    }

    public void setPlafondPaiement(Double plafondPaiement) {
        this.plafondPaiement = plafondPaiement;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 79 * hash + (this.compteCarte != null ? this.compteCarte.hashCode() : 0);
        hash = 79 * hash + (this.numero != null ? this.numero.hashCode() : 0);
        hash = 79 * hash + (this.cryptogramme != null ? this.cryptogramme.hashCode() : 0);
        hash = 79 * hash + (this.dateExpiration != null ? this.dateExpiration.hashCode() : 0);
        hash = 79 * hash + (this.fraisAnnuel != null ? this.fraisAnnuel.hashCode() : 0);
        hash = 79 * hash + (this.plafondRetrait != null ? this.plafondRetrait.hashCode() : 0);
        return hash;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CarteEntity other = (CarteEntity) obj;
        return this.id == other.id || (this.id != null && this.id.equals(other.id));
    }
    
  
}
