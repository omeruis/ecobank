/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name="Chequier")
public class ChequeEntity implements Serializable {
    public enum typeCheque{CHEQUE_BARRE, CHEQUE_NON_BARRE}

    public enum Status{EN_DEMANDE, LIVRE}

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "type_cheque", nullable = false)
    private typeCheque type;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @ManyToOne
    @JoinColumn(name="numero_compte")
    private CompteCourantEntity compteCible;

    public ChequeEntity(typeCheque type, Status status, CompteCourantEntity compteCible) {
        this.type = type;
        this.status = status;
        this.compteCible = compteCible;
    }

    public ChequeEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public typeCheque getType() {
        return type;
    }

    public void setType(typeCheque type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public CompteCourantEntity getCompteCible() {
        return compteCible;
    }

    public void setCompteCible(CompteCourantEntity compteCible) {
        this.compteCible = compteCible;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 37 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 37 * hash + (this.status != null ? this.status.hashCode() : 0);
        hash = 37 * hash + (this.compteCible != null ? this.compteCible.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChequeEntity other = (ChequeEntity) obj;
        return this.id == other.id || (this.id != null && this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "ChequeEntity{" + "id=" + id + ", type=" + type + ", status=" + status + ", compteCible=" + compteCible + '}';
    }

}
