/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import models.Gender;
import models.MemberSubscribeForm;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name = "Client")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class ClientEntity implements Serializable {

    public enum Type {
        PARTICULIER,
        PROFESSIONNEL;

        public static class Values {

            public static final String PARTICULIER = "PARTICULIER";
            public static final String PROFESSIONNEL = "PROFESSIONNEL";

            private Values() {
            }

        }
    }
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    protected Long id;

    @Column(nullable = false, name = "nom")
    protected String nom;

    @Column(nullable = false, name = "prenom")
    protected String prenom;

    @Column(name = "adresse")
    protected String adresse;

    @Column(name = "code_postale")
    protected String codePostale;

    @Column(name = "ville")
    protected String ville;

    @Column(name = "pays")
    protected String pays;

    @Column(name = "notifie")
    private Boolean notify;

    @Column(nullable = false, unique = true, name = "telephone")
    protected String telephone;

    @Column(nullable = false, unique = true, name = "email")
    protected String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "sexe")
    protected Gender sexe;

    @Column(nullable = true, length = 8, unique = true)
    protected String numero;

    @Column(nullable = true)
    protected String code;

    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "conseiller_id")
    protected ConseillerEntity conseiller;

    @OneToMany(mappedBy = "clientPlanning")
    protected List<PlanningEntity> plannings;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "box")
    protected MessageBoxEntity messageBox;

    @ManyToMany(mappedBy = "clients")
    protected List<CompteEntity> comptes;

    @OneToMany(mappedBy = "clientNotification", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    protected List<NotificationEntity> notifications;
    
    @OneToMany(mappedBy = "clientBeneficiaire", fetch=FetchType.LAZY)
    private List<BeneficiaireEntity> beneficiaires;

    public ClientEntity() {
        this.plannings = new ArrayList<PlanningEntity>();
        this.comptes = new ArrayList<CompteEntity>();
        this.notifications = new ArrayList<NotificationEntity>();
    }

    public ClientEntity(String nom, String prenom, String adresse, String codePostale, String ville, String pays, String telephone, Gender sexe, String email) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostale = codePostale;
        this.ville = ville;
        this.pays = pays;
        this.telephone = telephone;
        this.email = email;
        this.sexe = sexe;
        this.numero = "";
        this.code = "";
        this.plannings = new ArrayList<PlanningEntity>();
        this.comptes = new ArrayList<CompteEntity>();
        this.notifications = new ArrayList<NotificationEntity>();
    }

    public ClientEntity(MemberSubscribeForm form) {
        this.nom = form.getLast_name();
        this.prenom = form.getFirst_name();
        this.adresse = form.getAdress();
        this.codePostale = form.getPostal_code();
        this.ville = form.getCity();
        this.pays = form.getCountry();
        this.telephone = form.getPhone();
        this.sexe = form.getGender();
        this.email = form.getEmail();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(String codePostale) {
        this.codePostale = codePostale;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public Boolean getNotify() {
        return notify;
    }

    public void setNotify(Boolean notify) {
        this.notify = notify;
    }

    public MessageBoxEntity getMessageBox() {
        return messageBox;
    }

    public void setMessageBox(MessageBoxEntity messageBox) {
        this.messageBox = messageBox;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Gender getSexe() {
        return sexe;
    }

    public void setSexe(Gender sexe) {
        this.sexe = sexe;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ConseillerEntity getConseiller() {
        return conseiller;
    }

    public void setConseiller(ConseillerEntity conseiller) {
        this.conseiller = conseiller;
    }

    public List<PlanningEntity> getPlannings() {
        return plannings;
    }

    public void setPlannings(List<PlanningEntity> plannings) {
        this.plannings = plannings;
    }

    public List<CompteEntity> getComptes() {
        return comptes;
    }

    public void setComptes(List<CompteEntity> comptes) {
        this.comptes = comptes;
    }

    public List<NotificationEntity> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationEntity> notifications) {
        this.notifications = notifications;
    }

    public List<BeneficiaireEntity> getBeneficiaires() {
        return beneficiaires;
    }

    public void setBeneficiaires(List<BeneficiaireEntity> beneficiaires) {
        this.beneficiaires = beneficiaires;
    }

    public abstract Type getType();

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientEntity other = (ClientEntity) obj;
        return this.id == other.id;
    }

}
