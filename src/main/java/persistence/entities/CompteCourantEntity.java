/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 *
 * @author fnapporn
 */
@Entity
@Cacheable(false)
@DiscriminatorValue(value = CompteEntity.Type.Values.COMPTE_COURANT)
public class CompteCourantEntity extends CompteEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @OneToMany(mappedBy = "compteCible")
    private List<ChequeEntity> chequiers;
    
    @OneToMany(mappedBy = "beneficiaire")
    private List<VirementEntity> virements;
    
    @OneToMany(mappedBy = "compteCarte")
    private List<CarteEntity> cartes;

    @Column(name = "decouvert_autorise")
    private Double decouvertAutorise;

    public CompteCourantEntity() {
    }

    public CompteCourantEntity(Double decouvertAutorise, String numero, Double solde, Date dateOuverture, Double taux) {
        super(numero, solde, dateOuverture, taux);
        this.decouvertAutorise = decouvertAutorise;
    }

    public List<ChequeEntity> getChequiers() {
        return chequiers;
    }

    public void setChequiers(List<ChequeEntity> chequiers) {
        this.chequiers = chequiers;
    }

    public List<VirementEntity> getVirements() {
        return virements;
    }

    public void setVirements(List<VirementEntity> virements) {
        this.virements = virements;
    }

    public List<CarteEntity> getCartes() {
        return cartes;
    }

    public void setCartes(List<CarteEntity> cartes) {
        this.cartes = cartes;
    }

    public Double getDecouvertAutorise() {
        return decouvertAutorise;
    }

    public void setDecouvertAutorise(Double decouvertAutorise) {
        this.decouvertAutorise = decouvertAutorise;
    }

    @Override
     public Type getType() {
        return CompteEntity.Type.COMPTE_COURANT;
    }

    
           
}
