/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name = "Compte")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class CompteEntity implements Serializable {

    public enum Type {

        COMPTE_COURANT,
        COMPTE_EPARGNE,
        COMPTE_TITRE,
        COMPTE_JOIN;

        public static class Values {

            public static final String COMPTE_COURANT = "COMPTE_COURANT";
            public static final String COMPTE_EPARGNE = "COMPTE_EPARGNE";
            public static final String COMPTE_TITRE = "COMPTE_TITRE";
            public static final String COMPTE_JOIN = "COMPTE_JOIN";

            private Values() {
            }

        }
    }

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "numero", length = 11)
    protected String numero;

    @Column(name = "solde")
    protected Double solde;

    @Column(name = "date_ouverture")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date dateOuverture;

    @Column(name = "cle", nullable = true, length = 2)
    protected String cle;

    @Column(name = "code_guichet", nullable = true, length = 5)
    protected String codeGuichet;

    @Column(name = "code_bank", nullable = true, length = 5)
    protected String codeBank;

    @JoinTable(
            name = "client_compte",
            joinColumns = @JoinColumn(name = "numero"),
            inverseJoinColumns = @JoinColumn(name = "id")
    )
    @ManyToMany(fetch=FetchType.LAZY)
    protected List<ClientEntity> clients;

    @OneToMany(mappedBy = "compteLie", fetch=FetchType.LAZY)
    private List<OperationEntity> operations;
    
    @OneToMany(mappedBy = "compteCible", fetch=FetchType.LAZY)
    private List<BeneficiaireEntity> beneficiaires;

    public abstract Type getType();

    @Column(name = "taux")
    private Double taux;

    public CompteEntity(String numero, Double solde, Date dateOuverture, Double taux) {
        this.numero = numero;
        this.solde = solde;
        this.dateOuverture = dateOuverture;
        this.taux = taux;
    }

    public CompteEntity() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public Date getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    public String getCle() {
        return cle;
    }

    public void setCle(String cle) {
        this.cle = cle;
    }

    public String getCodeGuichet() {
        return codeGuichet;
    }

    public void setCodeGuichet(String codeGuichet) {
        this.codeGuichet = codeGuichet;
    }

    public String getCodeBank() {
        return codeBank;
    }

    public void setCodeBank(String codeBank) {
        this.codeBank = codeBank;
    }

    public List<ClientEntity> getClients() {
        return clients;
    }

    public void setClients(List<ClientEntity> clients) {
        this.clients = clients;
    }

    public List<OperationEntity> getOperations() {
        return operations;
    }

    public void setOperations(List<OperationEntity> operations) {
        this.operations = operations;
    }

    public Double getTaux() {
        return taux;
    }

    public void setTaux(Double taux) {
        this.taux = taux;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (this.numero != null ? this.numero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompteEntity other = (CompteEntity) obj;
        return this.numero == other.numero || (this.numero != null && this.numero.equals(other.numero));
    }

}
