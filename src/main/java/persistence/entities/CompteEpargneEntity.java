/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@DiscriminatorValue(value = CompteEntity.Type.Values.COMPTE_EPARGNE)
public class CompteEpargneEntity extends CompteEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Column(name = "plafond_depot")
    private Double plafondDepot;

    public CompteEpargneEntity(Double plafondDepot, String numero, Double solde, Date dateOuverture, Double taux) {
        super(numero, solde, dateOuverture, taux);
        this.plafondDepot = plafondDepot;
    }
    public CompteEpargneEntity() {
        super();
    }
    

    @Override
    public Type getType() {
        return CompteEntity.Type.COMPTE_EPARGNE;
    }

    public Double getPlafondDepot() {
        return plafondDepot;
    }

    public void setPlafondDepot(Double plafondDepot) {
        this.plafondDepot = plafondDepot;
    }
    
    
    
}
