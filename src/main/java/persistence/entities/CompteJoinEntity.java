/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author omer
 */
@Entity
@DiscriminatorValue(value = CompteEntity.Type.Values.COMPTE_JOIN)
public class CompteJoinEntity extends CompteEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    protected int maxOwner;

    @Override
    public Type getType() {
        return CompteEntity.Type.COMPTE_JOIN;
    }

    public int getMaxOwner() {
        return maxOwner;
    }

    public void setMaxOwner(int maxOwner) {
        this.maxOwner = maxOwner;
    }

    public CompteJoinEntity(String numero, Double solde, Date dateOuverture, Double taux) {
        super(numero, solde, dateOuverture, taux);
    }

    public CompteJoinEntity() {
    }
    
    
    
}
