/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 *
 * @author fnapporn
 */
@Entity
@DiscriminatorValue(value = CompteEntity.Type.Values.COMPTE_TITRE)
public class CompteTitreEntity extends CompteEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Column(name = "balance")
    private Double balance;
    
    @OneToMany(mappedBy = "compte")
    private List<OrdreEntity> ordres = new ArrayList<OrdreEntity>();    

    public CompteTitreEntity() {
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public List<OrdreEntity> getOrdres() {
        return ordres;
    }

    public void setOrdres(List<OrdreEntity> ordres) {
        this.ordres = ordres;
    }

    

    @Override
    public Type getType() {
        return CompteEntity.Type.COMPTE_TITRE;
    }    
    
}
