/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import models.Gender;
import models.ProSubscribeForm;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name = "Conseiller")
public class ConseillerEntity implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @Column(nullable = false, name = "nom")
    private String nom;
    
    @Column(nullable = false, name = "prenom")
    private String prenom;
    
    @Column(name = "adresse")
    private String adresse;
    
    @Column(name = "code_postale")
    private String codePostale;
    
    @Column(name = "ville")
    private String ville;
    
    @Column(name = "pays")
    protected String pays;
    
    @Column(nullable = false, unique=true, name = "telephone")
    private String telephone;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "sexe")
    private Gender sexe;
    
    @OneToMany(mappedBy = "conseillerNotification")
    private List<NotificationEntity> notifications;
    
    @Column(nullable = false, name = "username", unique = true)
    private String username;

    @Column(nullable = false, name = "password")
    private String password;

    @OneToMany(mappedBy = "conseiller")
    private List<ClientEntity> clients;
    
    @OneToMany(mappedBy = "conseillerPlanning")
    private List<PlanningEntity> plannings;
    
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "box")
    protected MessageBoxEntity messageBoxC;

    public ConseillerEntity() {
    }

    public ConseillerEntity(String nom, String prenom, String adresse, String codePostale, String ville, String pays, String telephone, Gender sexe, String username, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostale = codePostale;
        this.ville = ville;
        this.pays = pays;
        this.telephone = telephone;
        this.sexe = sexe;
        this.username = username;
        this.password = password;
    }

    public ConseillerEntity(ProSubscribeForm form) {
        this.nom = form.getLast_name();
        this.prenom = form.getFirst_name();
        this.adresse = form.getAdress();
        this.codePostale = form.getPostal_code();
        this.ville = form.getCity();
        this.sexe = form.getGender();
        this.telephone = form.getPhone();
        this.username = form.getEmail();
        this.password = form.getPasswd();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(String codePostale) {
        this.codePostale = codePostale;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Gender getSexe() {
        return sexe;
    }

    public void setSexe(Gender sexe) {
        this.sexe = sexe;
    }

    public List<NotificationEntity> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationEntity> notifications) {
        this.notifications = notifications;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<ClientEntity> getClients() {
        return clients;
    }

    public void setClients(List<ClientEntity> clients) {
        this.clients = clients;
    }

    public List<PlanningEntity> getPlannings() {
        return plannings;
    }

    public void setPlannings(List<PlanningEntity> plannings) {
        this.plannings = plannings;
    }

    public MessageBoxEntity getMessageBox() {
        return messageBoxC;
    }

    public void setMessageBox(MessageBoxEntity messageBox) {
        this.messageBoxC = messageBox;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        return hash;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConseillerEntity other = (ConseillerEntity) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "ConseillerEntity{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse + ", codePostale=" + codePostale + ", ville=" + ville + ", telephone=" + telephone + ", sexe=" + sexe + ", notifications=" + notifications + ", username=" + username + ", password=" + password + ", clients=" + clients + ", plannings=" + plannings + '}';
    }

    
    
    

}
