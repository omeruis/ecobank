/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name = "MessageBox")
public class MessageBoxEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "libelle", nullable = true)
    protected String libelle;
    
    @OneToMany(mappedBy = "emetteur")
    private List<MessageEntity> messagesEnvoye;
    
    @OneToMany(mappedBy = "destinataire")
    private List<MessageEntity> messagesRecu;
    
    @OneToOne(mappedBy = "messageBox", cascade = CascadeType.ALL)
    private ClientEntity client;
    
    @OneToOne(mappedBy = "messageBoxC", cascade = CascadeType.ALL)
    private ConseillerEntity conseiller;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<MessageEntity> getMessagesEnvoye() {
        return messagesEnvoye;
    }

    public void setMessagesEnvoye(List<MessageEntity> messagesEnvoye) {
        this.messagesEnvoye = messagesEnvoye;
    }

    public List<MessageEntity> getMessagesRecu() {
        return messagesRecu;
    }

    public void setMessagesRecu(List<MessageEntity> messagesRecu) {
        this.messagesRecu = messagesRecu;
    }

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity client) {
        this.client = client;
    }

    public ConseillerEntity getConseiller() {
        return conseiller;
    }

    public void setConseiller(ConseillerEntity conseiller) {
        this.conseiller = conseiller;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MessageBoxEntity other = (MessageBoxEntity) obj;
        return this.id == other.id || (this.id != null && this.id.equals(other.id));
    }
    
    
    
}
