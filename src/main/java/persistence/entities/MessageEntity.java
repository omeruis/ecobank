/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name = "message")
public class MessageEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "contenu")
    private String contenu;
    
    @Column(name = "titre")
    private String titre;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_message")
    private Date dateMessage;
    
    @ManyToOne
    @JoinColumn(name="emetteur")
    private MessageBoxEntity emetteur;
    
    @ManyToOne
    @JoinColumn(name="destinataire")
    private MessageBoxEntity destinataire;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDateMessage() {
        return dateMessage;
    }

    public void setDateMessage(Date dateMessage) {
        this.dateMessage = dateMessage;
    }


    public MessageEntity() {
    }

    public MessageEntity(String contenu, String titre, Date dateMessage, MessageBoxEntity emetteur, MessageBoxEntity destinataire) {
        this.contenu = contenu;
        this.titre = titre;
        this.dateMessage = dateMessage;
        this.emetteur = emetteur;
        this.destinataire = destinataire;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public MessageBoxEntity getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(MessageBoxEntity emetteur) {
        this.emetteur = emetteur;
    }

    public MessageBoxEntity getDestinataire() {
        return destinataire;
    }

    public void setDestinataire(MessageBoxEntity destinataire) {
        this.destinataire = destinataire;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MessageEntity other = (MessageEntity) obj;
        return this.id == other.id || (this.id != null && this.id.equals(other.id));
    }

    

}
