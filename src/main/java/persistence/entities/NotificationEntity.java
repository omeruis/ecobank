/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author omer
 */
@Entity
@Table(name = "Notification")
public class NotificationEntity implements Serializable {

    public enum typeNotification {
        ALERTE, MESSAGE, INFO
    }

    public enum Niveau {
        NEGLIGEABLE, IMPORNTANT, URGENT
    }

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @ManyToOne
    @JoinColumn(name="conseiller_id", nullable = true)
    private ConseillerEntity conseillerNotification;
    
    @ManyToOne
    @JoinColumn(name="client_id", nullable = true)
    private ClientEntity clientNotification;

    @Column(nullable = false, name = "message")
    private String message;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "type_notification")
    private typeNotification type;

    @Enumerated(EnumType.STRING)
    @Column(nullable = true, name = "importance")
    private Niveau importance;

    @Column(nullable = false, name = "date_notification")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateNotif;

    public NotificationEntity() {
        
    }

    public NotificationEntity(String message, typeNotification type, Date dateNotif) {
        this.message = message;
        this.type = type;
        this.dateNotif = dateNotif;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ConseillerEntity getConseillerNotification() {
        return conseillerNotification;
    }

    public void setConseillerNotification(ConseillerEntity conseillerNotification) {
        this.conseillerNotification = conseillerNotification;
    }

    public ClientEntity getClientNotification() {
        return clientNotification;
    }

    public void setClientNotification(ClientEntity clientNotification) {
        this.clientNotification = clientNotification;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public typeNotification getType() {
        return type;
    }

    public void setType(typeNotification type) {
        this.type = type;
    }

    public Niveau getImportance() {
        return importance;
    }

    public void setImportance(Niveau importance) {
        this.importance = importance;
    }

    public Date getDateNotif() {
        return dateNotif;
    }

    public void setDateNotif(Date dateNotif) {
        this.dateNotif = dateNotif;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotificationEntity other = (NotificationEntity) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "NotificationEntity{" + "id=" + id + '}';
    }

    

    

    

}
