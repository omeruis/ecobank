/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name="Operation")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class OperationEntity implements Serializable {
    public enum TypeOperation {DEBIT, CREDIT}

    private static final long serialVersionUID = 1L; 

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @ManyToOne
    @JoinColumn(name="numero_compte")
    private CompteEntity compteLie;
    
    @Column(name = "type_operation")
    @Enumerated(EnumType.STRING)
    private TypeOperation typeOperation;
    
    @Column(nullable = false, name = "montant")
    private Double montant;
    
    @Column(nullable = false, name = "date_operation")
    @Temporal(TemporalType.DATE)
    private Date dateOperation;
    
    @Column(name = "libelle")
    private String libelle;
    
    @Column(name = "description")
    private String description;

    public OperationEntity() {
    }

    public OperationEntity(CompteEntity compteLie, TypeOperation typeOperation, Double montant, Date dateOperation, String libelle) {
        this.compteLie = compteLie;
        this.typeOperation = typeOperation;
        this.montant = montant;
        this.dateOperation = dateOperation;
        this.libelle = libelle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public TypeOperation getTypeOperation() {
        return typeOperation;
    }

    public void setTypeOperation(TypeOperation typeOperation) {
        this.typeOperation = typeOperation;
    }    

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Date getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(Date dateOperation) {
        this.dateOperation = dateOperation;
    }

    public CompteEntity getCompteLie() {
        return compteLie;
    }

    public void setCompteLie(CompteEntity compteLie) {
        this.compteLie = compteLie;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OperationEntity other = (OperationEntity) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "OperationEntity{" + "id=" + id + ", compteLie=" + compteLie + ", typeOperation=" + typeOperation + ", montant=" + montant + ", dateOperation=" + dateOperation + '}';
    }
    
    
    
    
}
