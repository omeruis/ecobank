/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author omer
 */
@Entity
@Cacheable(false)
@Table(name = "Ordre")
public class OrdreEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name="numero_compte")
    private CompteTitreEntity compte;
    
    @ManyToOne
    @JoinColumn(name="action_id")
    private ActionEntity action;
    
    @Column(name = "quantite")
    private Integer quantite;

    public OrdreEntity() {}

    public OrdreEntity(Long id, CompteTitreEntity compte, ActionEntity action, Integer quantite) {
        this.id = id;
        this.compte = compte;
        this.action = action;
        this.quantite = quantite;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CompteTitreEntity getCompte() {
        return compte;
    }

    public void setCompte(CompteTitreEntity compte) {
        this.compte = compte;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdreEntity other = (OrdreEntity) obj;
        return this.id == other.id || (this.id != null && this.id.equals(other.id));
    }

}
