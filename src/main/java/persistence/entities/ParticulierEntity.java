/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import models.Gender;
import models.MemberSubscribeForm;

/**
 *
 * @author fnapporn
 */
@Entity
@DiscriminatorValue(value = ClientEntity.Type.Values.PARTICULIER)
public class ParticulierEntity extends ClientEntity{
    
    private static final long serialVersionUID = 1L;

    public ParticulierEntity(String nom, String prenom, String adresse, String codePostale, String ville, String pays, String telephone, Gender sexe, String email) {
        super(nom, prenom, adresse, codePostale, ville, pays, telephone, sexe, email);
    }

    public ParticulierEntity(MemberSubscribeForm form) {
        super(form);
    }  
    
    public ParticulierEntity() {
    }  
    
    private String situation;

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }
    
    @Override
    public Type getType() {
        return ClientEntity.Type.PARTICULIER;
    }

    @Override
    public String toString() {
        return "ParticulierEntity{" + "id=" + getId() + '}';
    }

}
