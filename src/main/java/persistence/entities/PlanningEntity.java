/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author omer
 */
@Entity
@Table(name="Planning")
public class PlanningEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @ManyToOne
    @JoinColumn(nullable = false, name="CONSEILLER_ID")
    private ConseillerEntity conseillerPlanning;
    
    @ManyToOne
    @JoinColumn(nullable = false, name="CLIENT_ID")
    private ClientEntity clientPlanning;
    
    @Column(nullable = false, name = "date_debut")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDebut;
    
    @Column(nullable = false, name = "date_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFin;
    
    @Column(nullable = true, name = "motif", length = 255 )
    private String motif;

    public PlanningEntity() {
    }

    public PlanningEntity(ConseillerEntity conseillerPlanning, ClientEntity clientPlanning, Date dateDebut, Date dateFin, String motif) {
        this.conseillerPlanning = conseillerPlanning;
        this.clientPlanning = clientPlanning;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.motif = motif;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ConseillerEntity getConseillerPlanning() {
        return conseillerPlanning;
    }

    public void setConseillerPlanning(ConseillerEntity conseillerPlanning) {
        this.conseillerPlanning = conseillerPlanning;
    }

    public ClientEntity getClientPlanning() {
        return clientPlanning;
    }

    public void setClientPlanning(ClientEntity clientPlanning) {
        this.clientPlanning = clientPlanning;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlanningEntity other = (PlanningEntity) obj;
        return this.id == other.id;
    }
    
    @Override
    public String toString() {
        return "PlanningEntity{" + "id=" + id + ", conseillerPlanning=" + conseillerPlanning + ", clientPlanning=" + clientPlanning + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", motif=" + motif + '}';
    }
    
}
