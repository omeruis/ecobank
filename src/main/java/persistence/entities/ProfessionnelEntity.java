/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import models.Gender;
import models.MemberSubscribeForm;

/**
 *
 * @author fnapporn
 */
@Entity
@DiscriminatorValue(value = ClientEntity.Type.Values.PROFESSIONNEL)
public class ProfessionnelEntity extends ClientEntity implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @Column(name = "siret")
    private String siret;
    
    @Column(name = "max_prelevement")
    private int nb_prelevement;
    
    @Column(name = "status")
    private String status;

    public ProfessionnelEntity(String nom, String prenom, String adresse, String codePostale, String ville, String pays, String telephone, Gender sexe, String email) {
        super(nom, prenom, adresse, codePostale, ville, pays, telephone, sexe, email);
    }

    public ProfessionnelEntity() {
    }
    
    public ProfessionnelEntity(MemberSubscribeForm form) {
        super(form);
    }  

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public int getNb_prelevement() {
        return nb_prelevement;
    }

    public void setNb_prelevement(int nb_prelevement) {
        this.nb_prelevement = nb_prelevement;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }    
    
    @Override
    public Type getType() {
        return ClientEntity.Type.PROFESSIONNEL;
    }

    @Override
    public String toString() {
        return "ProfessionnelEntity{" + "id=" + getId() + '}';
    }
    
    
}
