/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import models.Situation;
import static models.Situation.CELIBATAIRE;
import static models.Situation.DIVORCE;
import static models.Situation.MARIE;
import static models.Situation.VEUF;

/**
 *
 * @author omer
 */
@Converter(autoApply = true)
public class SituationConverter implements AttributeConverter<Situation, String> {

    @Override
    public String convertToDatabaseColumn(Situation attribute) {
        switch (attribute) {
            case CELIBATAIRE:
                return "C";
            case MARIE:
                return "M";
            case DIVORCE:
                return "D";
            case VEUF:
                return "V";
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }}

    /**
     *
     * @param dbData
     * @return
     */
    @Override
    public Situation convertToEntityAttribute(String dbData) {
        if(dbData.equals("C")){
            return CELIBATAIRE;
        }else if(dbData.equals("M")){
            return MARIE;
        }else if(dbData.equals("D")){
            return DIVORCE;
        }else if(dbData.equals("V")){
            return VEUF;
        }else{
            throw new IllegalArgumentException("Unknown" + dbData);
        }
    }
}
