/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author omer
 */
@Entity
@Table(name = "Virement")
public class VirementEntity extends OperationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name="beneficiaire")
    private CompteEntity beneficiaire;

    public CompteEntity getBeneficiaire() {
        return beneficiaire;
    }

    public void setBeneficiaire(CompteEntity beneficiaire) {
        this.beneficiaire = beneficiaire;
    }

    public VirementEntity() {
        super();
    }

    public VirementEntity(CompteEntity beneficiaire, CompteEntity compteLie, TypeOperation typeOperation, Double montant, Date dateOperation, String libelle) {
        super(compteLie, typeOperation, montant, dateOperation, libelle);
        this.beneficiaire = beneficiaire;
    }

}
