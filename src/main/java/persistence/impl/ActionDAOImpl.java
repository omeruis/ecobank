/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import persistence.dao.AbstractDAO;
import persistence.dao.ActionDAO;
import persistence.entities.ActionEntity;

/**
 *
 * @author omer
 */
@Repository
public class ActionDAOImpl extends AbstractDAO<ActionEntity> implements ActionDAO{

    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;
    
    public ActionDAOImpl() {
        super(ActionEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
