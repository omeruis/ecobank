/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import persistence.dao.AbstractDAO;
import persistence.dao.BeneficiaireDAO;
import persistence.entities.BeneficiaireEntity;

/**
 *
 * @author omer
 */
@Repository
public class BeneficiaireDAOImpl extends AbstractDAO<BeneficiaireEntity> implements BeneficiaireDAO{

    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;
    
    public BeneficiaireDAOImpl() {
        super(BeneficiaireEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<BeneficiaireEntity>  findByNumero(String numero) {
        try{
            Query q = em.createQuery("SELECT b FROM BeneficiaireEntity b WHERE b.compteCible.numero= ?1").setParameter(1,numero);
            return q.getResultList();
        }catch(NoResultException e) {
        return null;
        }
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<BeneficiaireEntity> findByClientId(Long id){
        try{
            Query q = em.createQuery("SELECT b FROM BeneficiaireEntity b WHERE b.clientBeneficiaire.id = ?1").setParameter(1,id);
            return q.getResultList();
        }catch(NoResultException e) {
        return null;
        }
    }
    
}
