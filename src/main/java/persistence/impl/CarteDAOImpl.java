/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import persistence.dao.AbstractDAO;
import persistence.entities.CarteEntity;
import persistence.dao.CarteDAO;

/**
 *
 * @author omer
 */
@Repository
public class CarteDAOImpl extends AbstractDAO<CarteEntity>  implements CarteDAO{
   
    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public CarteDAOImpl() {
        super(CarteEntity.class);
    }
    
    @Transactional(readOnly = true)
    @Override
    public CarteEntity findByNumero(String numero) {
        Query q = em.createQuery("SELECT c FROM CarteEntity c WHERE c.numero= ?").setParameter(1,numero);
        return (CarteEntity) q.getSingleResult();
    }

    @Transactional(readOnly = true)
    @Override
    public List<CarteEntity> findByNumeroCompte(String numero) {
        Query q = em.createQuery("SELECT c FROM CarteEntity c WHERE c.numero_compte= ?").setParameter(1,numero);
        return q.getResultList();
    }
    
}
