/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import persistence.dao.AbstractDAO;
import persistence.dao.ChequeDAO;
import persistence.entities.ChequeEntity;

/**
 *
 * @author omer
 */
@Repository
public class ChequeDAOImpl extends AbstractDAO<ChequeEntity> implements ChequeDAO{
    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;

    public ChequeDAOImpl() {
        super(ChequeEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
