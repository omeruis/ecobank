/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import persistence.dao.AbstractDAO;
import persistence.dao.ClientDAO;
import persistence.entities.ClientEntity;

/**
 * 
 * @author omer
 */
@Repository
public class ClientDAOImpl extends AbstractDAO<ClientEntity> implements ClientDAO {

    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClientDAOImpl() {
        super(ClientEntity.class);
    }
    
    /*@Transactional
    @Override
    public void create(ClientEntity entity) {
        entity = em.merge(entity);
        em.persist(entity);
        em.flush();
    }*/
    
    @Override
    public ClientEntity find(Object id){
        return super.find(id);
    }
    
    @Transactional(readOnly = true)
    @Override
    public ClientEntity findByNumero(String numero) {
        try{
            Query q = em.createQuery("SELECT c FROM ClientEntity c WHERE c.numero= ?1").setParameter(1,numero);
            return (ClientEntity) q.getSingleResult();
        }catch(NoResultException e) {
        return null;
        }
    }

    
    
}
