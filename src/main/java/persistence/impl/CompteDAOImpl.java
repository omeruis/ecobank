/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import persistence.dao.AbstractDAO;
import persistence.dao.CompteDAO;
import persistence.entities.CompteEntity;

/**
 *
 * @author omer
 */
@Repository
public class CompteDAOImpl extends AbstractDAO<CompteEntity> implements CompteDAO{
    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;

    public CompteDAOImpl() {
        super(CompteEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @Transactional(readOnly = true)
    @Override
    public CompteEntity findByNumero(String numero) {
        try{
            Query q = em.createQuery("SELECT c FROM CompteEntity c WHERE c.numero= ?1").setParameter(1,numero);
            return (CompteEntity) q.getSingleResult();
        }catch(NoResultException e) {
        return null;
        }
    }
}
