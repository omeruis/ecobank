/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import persistence.dao.AbstractDAO;
import persistence.dao.ConseillerDAO;
import persistence.entities.ConseillerEntity;

/**
 *
 * @author omer
 */
@Repository
public class ConseillerDAOImpl extends AbstractDAO<ConseillerEntity> implements ConseillerDAO {

    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ConseillerDAOImpl() {
        super(ConseillerEntity.class);
    }

    @Transactional(readOnly = true)
    @Override
    public ConseillerEntity findByUserName(String userName) {
        Query q = em.createQuery("SELECT c FROM ConseillerEntity c WHERE c.username= ?1").setParameter(1, userName);
        try {
            return (ConseillerEntity) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
}
