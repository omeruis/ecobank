/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import persistence.dao.AbstractDAO;
import persistence.dao.MessageBoxDAO;
import persistence.entities.MessageBoxEntity;

/**
 *
 * @author omer
 */
@Repository
public class MessageBoxDAOImpl extends AbstractDAO<MessageBoxEntity> implements MessageBoxDAO{
    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;

    public MessageBoxDAOImpl() {
        super(MessageBoxEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
