/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import persistence.dao.AbstractDAO;
import persistence.dao.MessageDAO;
import persistence.entities.MessageEntity;

/**
 *
 * @author omer
 */
@Repository
public class MessageDAOImpl extends AbstractDAO<MessageEntity> implements MessageDAO{
    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;

    public MessageDAOImpl() {
        super(MessageEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}

