/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import persistence.dao.AbstractDAO;
import persistence.dao.OperationDAO;
import persistence.entities.OperationEntity;

/**
 *
 * @author omer
 */
@Repository
public class OperationDAOImpl extends AbstractDAO<OperationEntity> implements OperationDAO{
    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;
    
    public OperationDAOImpl() {
        super(OperationEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
