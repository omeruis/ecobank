package persistence.impl;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import persistence.dao.AbstractDAO;
import persistence.dao.OrdreDAO;
import persistence.entities.OrdreEntity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author omer
 */
@Repository
public class OrdreDAOImpl extends AbstractDAO<OrdreEntity> implements OrdreDAO{
    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;

    public OrdreDAOImpl() {
        super(OrdreEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
