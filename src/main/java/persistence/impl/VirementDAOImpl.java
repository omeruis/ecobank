/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import persistence.dao.AbstractDAO;
import persistence.dao.VirementDAO;
import persistence.entities.VirementEntity;

/**
 *
 * @author omer
 */
@Repository
public class VirementDAOImpl extends AbstractDAO<VirementEntity> implements VirementDAO{
    @PersistenceContext(unitName = "ecoBankPU")
    private EntityManager em;
    
    public VirementDAOImpl() {
        super(VirementEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<VirementEntity> findByNumeroBeneficiaire(String numero){
        try{
            Query q = em.createQuery("SELECT v FROM VirementEntity v WHERE v.beneficiaire.numero = ?1").setParameter(1, numero);
            return q.getResultList();
        }catch(NoResultException e) {
        return null;
        }
    }
    
}
