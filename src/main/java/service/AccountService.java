/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import models.AccountCard;
import models.AccountDetail;
import models.AccountType;
import models.Beneficiaire;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lavenant
 */
@Service
public interface AccountService {
    List<AccountCard> GetMemberAccounts(String numero);
    boolean CreateAccount(AccountType type);
    int GetAccountDetailPageNumber(String parameter);
    AccountDetail GetAccountDetail(String parameter, int currentPage);
    List<Beneficiaire> GetMemberBeneficiaires();
    boolean CreateTransaction(String beneficiaireNum, String emetteurNum, double ammount, String titre, String detail);
    boolean Prelevement(String numero, Double ammount, String titre, String detail);
    boolean CreateBeneficiaire(String benefName, String benefAccNum, String benefIban);
}
