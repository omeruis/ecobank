/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import org.springframework.stereotype.Service;
import persistence.entities.ClientEntity;

/**
 *
 * @author omer
 */
@Service
public interface ClientService {
    boolean TryConnectMember(String numero, String code);
    boolean searchByNumero(String numero);
    Long getClientId();
    ClientEntity getClient(Long clientId);
}
