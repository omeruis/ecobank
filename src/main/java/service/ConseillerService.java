/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import models.Client;
import org.springframework.stereotype.Service;
import persistence.entities.ConseillerEntity;

/**
 *
 * @author omer
 */
@Service
public interface ConseillerService {
    boolean TryConnectAdvisor(String userName, String password);
    boolean searchByUserName(String userName);
    Integer getConseillerId();
    ConseillerEntity getConseiller(Integer id);
    
    List<Client> getConseillerClients(int page);
    int getConseillerClientsPageNumber();
}
