/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import org.springframework.stereotype.Service;

/**
 *
 * @author Lavenant
 */
@Service
public interface DocumentService {

    boolean CreateAskCard(String accountNumber);

    boolean CreateAskCheck(String accountNumber);

    String GetAccountResume(String accountNumber);
    
}
