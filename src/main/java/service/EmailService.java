/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.List;
import models.Mail;
import models.UserKind;
import org.springframework.stereotype.Service;

/**
 *
 * @author omer
 */
@Service
public interface EmailService {

    /**
     * Service d'envoi de mail
     * @param toAddress
     * @param subject
     * @param message
     */
    void sendEmail(String toAddress, String subject, String message);
    
    boolean sendInternalEmail(String clientId, String subject, String message);
    boolean sendInternalEmail(String subject, String message);
    int getInternalEmailPageNum();
    List<Mail> getInternalEmailPage(int page, UserKind userKind);
}
