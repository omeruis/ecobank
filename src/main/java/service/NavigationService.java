/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import models.NavigationContainer;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lavenant
 */
@Service
public interface NavigationService {
    NavigationContainer ComputeNavigation(int numberOfPages, String page) throws NumberFormatException;
    int GetMaxAccountDetailPerPage();
    int GetMaxMailPerPage();
    int GetClientPerPage();
}
