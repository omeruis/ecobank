/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import models.MemberSubscribeForm;
import models.ProSubscribeForm;
import org.springframework.stereotype.Service;

/**
 *
 * @author tlavenan
 */
@Service
public interface SubscribeService {

    boolean SubscribeMember(MemberSubscribeForm form);
    boolean MemberFormPasswordEquals(MemberSubscribeForm form);
    boolean MemberFormWellFilled(MemberSubscribeForm form);
    
    boolean SubscribePro(ProSubscribeForm form);
    boolean ProFormPasswordEquals(ProSubscribeForm form);
    boolean ProFormWellFilled(ProSubscribeForm form);

    boolean UpdateMember(MemberSubscribeForm formData);

    MemberSubscribeForm GetUserData(Object attribute);
    boolean UpdateAdvisor(ProSubscribeForm formData);
    ProSubscribeForm GetAdvisorData(Object attribute);
}
