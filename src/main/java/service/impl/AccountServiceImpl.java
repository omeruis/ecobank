/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import static java.lang.Math.ceil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import models.AccountCard;
import models.AccountDetail;
import models.AccountOperation;
import models.AccountType;
import models.Beneficiaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import persistence.dao.BeneficiaireDAO;
import persistence.dao.ClientDAO;
import persistence.dao.CompteDAO;
import persistence.dao.OperationDAO;
import persistence.dao.VirementDAO;
import persistence.entities.BeneficiaireEntity;
import persistence.entities.ClientEntity;
import persistence.entities.CompteCourantEntity;
import persistence.entities.CompteEntity;
import persistence.entities.CompteEpargneEntity;
import persistence.entities.OperationEntity;
import persistence.entities.VirementEntity;
import service.AccountService;
import service.ClientService;
import service.NavigationService;
import service.utils.AcountToCard;
import service.utils.OperationToAcountOperation;

/**
 * Service that handle all the business logic for the accounts
 * @author Lavenant
 */
@Service
public class AccountServiceImpl implements AccountService {

    //==========================================================================
    //Atributes

    @Autowired
    ClientService clientservice;
    
    @Autowired
    NavigationService navigationService;

    @Resource
    CompteDAO dao;

    @Resource
    ClientDAO daoClient;

    @Resource
    CompteDAO daoCompte;

    @Resource
    BeneficiaireDAO daoBenef;

    @Resource
    VirementDAO daoVirement;
    
    @Resource
    OperationDAO daoOperation;

    //==========================================================================
    // Ctor
    public AccountServiceImpl() {
    }

    //==========================================================================
    //Impl

    /**
     * Returns the member account list
     * @param numero
     * @return
     */
    @Override
    public List<AccountCard> GetMemberAccounts(String numero) {
        ClientEntity client = daoClient.findByNumero(numero);
        List<AccountCard> accounts = new ArrayList<AccountCard>();
        for (CompteEntity compte : client.getComptes()) {
            AccountCard c = AcountToCard.ConvertToCard(compte);
            accounts.add(c);
        }
        return accounts;
    }

    /**
     * Creates an account of the desired type
     * @param type
     * @return
     */
    @Override
    public boolean CreateAccount(AccountType type) {
        if (type.equals(AccountType.CURENT)) {
            Random r = new Random();
            Integer nb1 = 1000000 + r.nextInt(8999999);
            Integer nb2 = 1000 + r.nextInt(8999);
            String numero = nb1.toString() + "" + nb2.toString();
            Double taux = new Double("0.01");
            Double decouvert = new Double("100");
            Double solde = new Double("2000.0");
            Date date = new Date();
            CompteEntity c = new CompteCourantEntity(decouvert, numero, solde, date, taux);
            c.setCle("79");
            c.setCodeGuichet("01276");
            c.setCodeBank("30004");
            c.setClients(Arrays.asList(clientservice.getClient(clientservice.getClientId())));
            dao.create(c);
            return true;
        }

        if (type.equals(AccountType.SAVE)) {
            Random r = new Random();
            Integer nb1 = 1000000 + r.nextInt(8999999);
            Integer nb2 = 1000 + r.nextInt(8999);
            String numero = nb1.toString() + "" + nb2.toString();
            Double taux = new Double("0.075");
            Double plafond = new Double("10000");
            Double solde = new Double("1000.0");
            Date date = new Date();
            CompteEntity c = new CompteEpargneEntity(plafond, numero, solde, date, taux);
            c.setClients(Arrays.asList(clientservice.getClient(clientservice.getClientId())));
            dao.create(c);
            return true;
        }

        return type.equals(AccountType.STOCK);
    }

    /**
     * Gest the detail page number to display it page by page
     * @param parameter
     * @return
     */
    @Override
    public int GetAccountDetailPageNumber(String parameter) {
        CompteEntity c = daoCompte.findByNumero(parameter);
        double nb = ((c.getOperations().size()+daoVirement.findByNumeroBeneficiaire(parameter).size()+0.0) / navigationService.GetMaxAccountDetailPerPage());
        return (int) ceil(nb);
    }

    /**
     * Returns the account detail
     * @param parameter
     * @param currentPage
     * @return
     */
    @Override
    public AccountDetail GetAccountDetail(String parameter, int currentPage) {
        CompteEntity c = daoCompte.findByNumero(parameter);
        AccountDetail retVal = new AccountDetail();
        
        retVal.setAmount(c.getSolde());
        retVal.setNumber(c.getNumero());
        List<AccountOperation> ops = new ArrayList<AccountOperation>();
        for (OperationEntity operation : c.getOperations()) {
            AccountOperation op = OperationToAcountOperation.OperationConvert(operation);
            ops.add(op);
        }
        List<VirementEntity> virements = daoVirement.findByNumeroBeneficiaire(parameter);
        for (VirementEntity operation : virements) {
            AccountOperation op = OperationToAcountOperation.VirementConvert(operation);
            op.setAmount(-op.getAmount());
            ops.add(op);
        }
        ops.sort(new Comparator<AccountOperation>(){
            @Override
            public int compare(AccountOperation o1, AccountOperation o2) {
                SimpleDateFormat parser=new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
                try {
                    return parser.parse(o1.getDate()).compareTo(parser.parse(o2.getDate()));
                } catch (ParseException ex) {
                    Logger.getLogger(AccountServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                    return 0;
                }
            }
        });
        int last = (currentPage*navigationService.GetMaxAccountDetailPerPage());
        if (last > ops.size()){
            last = ops.size();
        }
        retVal.setOperations(ops.subList((currentPage-1)*navigationService.GetMaxAccountDetailPerPage(), last));

        return retVal;
    }

    /**
     * Returns the beneficiaires list for this client
     * @return
     */
    @Override
    public List<Beneficiaire> GetMemberBeneficiaires() {
        List<Beneficiaire> retVal = new ArrayList<Beneficiaire>();
        ClientEntity c = clientservice.getClient(clientservice.getClientId());
        for (BeneficiaireEntity b : c.getBeneficiaires()) {
            Beneficiaire beneficiaire = new Beneficiaire();
            beneficiaire.setNom(b.getClient().getNom()+" : "+b.getCompteCible().getNumero());
            beneficiaire.setAccountNumber(b.getCompteCible().getNumero());
            retVal.add(beneficiaire);
        }
        return retVal;
    }

    /**
     * Creates a transaction (virement)
     * @param beneficiaireNum
     * @param emetteurNum
     * @param ammount
     * @param libelle
     * @param detail
     * @return
     */
    @Override
    public boolean CreateTransaction(String beneficiaireNum, String emetteurNum, double ammount, String libelle, String detail) {
        if (!beneficiaireNum.isEmpty() && !emetteurNum.equals(beneficiaireNum)) {
            CompteEntity compteBeneficiaire = daoCompte.findByNumero(beneficiaireNum);
            CompteEntity compteEmetteur = daoCompte.findByNumero(emetteurNum);
            VirementEntity v = new VirementEntity();
            v.setBeneficiaire(compteBeneficiaire);
            v.setCompteLie(compteEmetteur);
            v.setMontant(-ammount);
            v.setTypeOperation(OperationEntity.TypeOperation.DEBIT);
            v.setDateOperation(new Date());
            v.setLibelle(libelle);
            v.setDescription(detail);
            daoVirement.create(v);
            compteBeneficiaire.setSolde(compteBeneficiaire.getSolde()+ammount);
            compteEmetteur.setSolde(compteEmetteur.getSolde()-ammount);
            daoCompte.update(compteEmetteur);
            daoCompte.update(compteBeneficiaire);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Creates a beneficiaire to send a transaction to
     * @param benefName
     * @param benefAccNum
     * @param benefIban
     * @return
     */
    @Override
    public boolean CreateBeneficiaire(String benefName, String benefAccNum, String benefIban) {
        if (!benefAccNum.isEmpty()) {
            ClientEntity client = clientservice.getClient(clientservice.getClientId());
            CompteEntity compte = daoCompte.findByNumero(benefAccNum);
            BeneficiaireEntity benef = new BeneficiaireEntity(client, compte);
            benef.setDateAjout(new Date());
            daoBenef.create(benef);
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Take money away from an account
     * @param numero
     * @param ammount
     * @param titre
     * @param detail
     * @return
     */
    @Override
    public boolean Prelevement(String numero, Double ammount, String titre, String detail){
         if (!numero.isEmpty()) {
             CompteEntity compte = daoCompte.findByNumero(numero);
             OperationEntity op = new OperationEntity();
             op.setCompteLie(compte);
             op.setDateOperation(new Date());
             op.setLibelle(titre);
             op.setDescription(detail);
             op.setMontant(-ammount);
             op.setTypeOperation(OperationEntity.TypeOperation.DEBIT);
             daoOperation.create(op);
             compte.setSolde(compte.getSolde()-ammount);
             daoCompte.update(compte);
             return true;
         } else {
            return false;
         }
    }
}
