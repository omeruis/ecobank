/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import persistence.dao.ClientDAO;
import service.ClientService;
import persistence.entities.ClientEntity;

/**
 * Handle client business logic.
 * @author Lavenant
 */
@Service
public class ClientServiceImpl implements ClientService {
    //==========================================================================
    //Atributes
    private ClientEntity connectedClient;
    
    @Resource
    ClientDAO dao;
    
    //==========================================================================
    //Methods

    /**
     * Tries to connect
     * @param numero
     * @param code
     * @return
     */
    @Override
    public boolean TryConnectMember(String numero, String code) {
        connectedClient = dao.findByNumero(numero);
        if (connectedClient == null) {
            return false;
        }
        boolean retour = connectedClient.getCode().equals(SubscribeServiceImpl.hashToSHA1(code));
        if (retour) {
            connectedClient = dao.findByNumero(connectedClient.getNumero());
        }
        return retour;
    }

    @Override
    public boolean searchByNumero(String numero) {
        return dao.findByNumero(numero) != null;
    }

    @Override
    public Long getClientId() {
        return connectedClient.getId();
    }

    @Override
    public ClientEntity getClient(Long clientId) {
        return dao.find(clientId);
    }
}
