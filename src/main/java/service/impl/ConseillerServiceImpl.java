/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import static java.lang.Math.ceil;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import models.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persistence.dao.ConseillerDAO;
import persistence.entities.ClientEntity;
import persistence.entities.ConseillerEntity;
import service.ConseillerService;
import service.NavigationService;

/**
 * Handles the connection logic for the advisor
 * @author omer
 */
@Service
public class ConseillerServiceImpl implements ConseillerService {
    //==========================================================================
    //Atributes
    
    private ConseillerEntity connectedConseiller;

    @Autowired
    NavigationService navigationService;

    @Resource
    ConseillerDAO dao;

    //==========================================================================
    //Methods

    /**
     * Try to log on
     * @param userName
     * @param password
     * @return
     */
    @Override
    public boolean TryConnectAdvisor(String userName, String password) {
        connectedConseiller = dao.findByUserName(userName);
        if (connectedConseiller == null) {
            return false;
        }
        boolean retour = connectedConseiller.getPassword().equals(SubscribeServiceImpl.hashToSHA1(password));
        if (retour) {
            connectedConseiller = dao.findByUserName(connectedConseiller.getUsername());
        }
        return retour;
    }

    @Override
    public boolean searchByUserName(String userName) {
        return dao.findByUserName(userName) != null;
    }

    @Override
    public Integer getConseillerId() {
        return connectedConseiller.getId();
    }

    @Override
    public ConseillerEntity getConseiller(Integer id) {
        return dao.find(id);
    }

    /**
     * Returns the list of clients for this advisor
     * @param page
     * @return
     */
    @Override
    public List<Client> getConseillerClients(int page) {
        List<Client> retVal = new ArrayList<>();
        List<ClientEntity> clients = getConseiller(getConseillerId()).getClients();
        for (ClientEntity client : clients) {
            Client c = new Client();
            c.setId(client.getId().toString());
            c.setName(client.getNom());
            c.setPrenom(client.getPrenom());
            c.setSoldeGlobal(14.23);//TODO
            retVal.add(c);
        }

        if (page != 0) {
            int last = (page * navigationService.GetClientPerPage());
            if (last > retVal.size()) {
                last = retVal.size();
            }
            return retVal.subList((page - 1) * navigationService.GetClientPerPage(), last);
        }
        else {
            return retVal;
        }
    }

    /**
     * Returns the number of pages full of clients for this advisor
     * @return
     */
    @Override
    public int getConseillerClientsPageNumber() {
        List<ClientEntity> clients = getConseiller(getConseillerId()).getClients();
        double nb = ((clients.size() + 0.0) / navigationService.GetClientPerPage());
        return (int) ceil(nb);
    }

}
