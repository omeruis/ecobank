/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import java.util.Date;
import java.util.Random;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persistence.dao.CarteDAO;
import persistence.dao.CompteDAO;
import persistence.entities.CarteEntity;
import persistence.entities.CompteCourantEntity;
import service.AccountService;
import service.DocumentService;

/**
 * Handle all the documents logic
 * @author Lavenant
 */
@Service
public class DocumentServiceImpl implements DocumentService {
    //==========================================================================
    //Atributes
    
    @Autowired
    private AccountService accountService;
    
    @Resource
    CompteDAO daoCompte;

    @Resource
    CarteDAO daoCarte;

    //==========================================================================
    //Methods

    /**
     * Ask for a new banking card
     * @param accountNumber
     * @return
     */
    @Override
    public boolean CreateAskCard(String accountNumber) {
        Random r = new Random();
        Integer nb1 = 100 + r.nextInt(899);
        Double min = new Double("1000000000000000");
        Double max = new Double("8999999999999999");
        Double randomValue = min + (max - min) * r.nextDouble();
        CompteCourantEntity c = (CompteCourantEntity) daoCompte.findByNumero(accountNumber);

        CarteEntity carte = new CarteEntity();
        carte.setCryptogramme(nb1.toString());
        carte.setDateExpiration(new Date(2022, 12, 31));
        carte.setFraisAnnuel(new Double("27.50"));
        carte.setNumero(randomValue.toString());
        carte.setPlafondPaiement(new Double("300"));
        carte.setPlafondRetrait(new Double("500"));
        carte.setCompteCarte(c);
        carte.setTypeCarte(CarteEntity.TypeCarte.VISA);
        
        daoCarte.create(carte);
        return accountService.Prelevement(accountNumber, new Double("9"), "Demande de carte", "Prelevement pour demande de carte VISA");
    }

    @Override
    public boolean CreateAskCheck(String accountNumber) {
        return true;
    }

    @Override
    public String GetAccountResume(String accountNumber) {
        return "";
    }
}
