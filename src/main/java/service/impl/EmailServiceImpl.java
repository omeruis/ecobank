/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import models.Mail;
import models.UserKind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persistence.dao.ClientDAO;
import persistence.dao.ConseillerDAO;
import persistence.dao.MessageDAO;
import persistence.entities.ClientEntity;
import persistence.entities.ConseillerEntity;
import persistence.entities.MessageEntity;
import service.ClientService;
import service.ConseillerService;
import service.EmailService;

/**
 * Handle mail business logic
 * @author omer
 */
@Service
public class EmailServiceImpl implements EmailService {
    //==========================================================================
    //Atributes
    
    @Autowired
    private ClientService clientService;

    @Autowired
    private ConseillerService conseillerService;

    @Resource
    ClientDAO daoClient;

    @Resource
    ConseillerDAO daoConseiller;

    @Resource
    MessageDAO daoMessage;

    private final String host = "mx1.hostinger.fr";
    private final String port = "587";
    private final String user = "chorinitia@preprod.thibautlavenant.com";
    private final String pass = "sCqIT0TUtnDv";

    //==========================================================================
    //Methods
    
    /**
     * Sends email through mailing server
     * @param toAddress
     * @param subject
     * @param message 
     */
    @Override
    public void sendEmail(String toAddress, String subject, String message) {

        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pass);
            }
        };

        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(user));
            InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
            msg.setRecipients(Message.RecipientType.TO, toAddresses);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setText(message);

            // sends the e-mail
            Transport.send(msg);
        } catch (MessagingException ex) {
            Logger.getLogger(EmailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * From advisor to client
     *
     * @param client
     * @param subject
     * @param message
     * @return
     */
    @Override
    public boolean sendInternalEmail(String client, String subject, String message) {
        if (!client.isEmpty()) {
            ConseillerEntity conseiller = conseillerService.getConseiller(conseillerService.getConseillerId());
            ClientEntity c = daoClient.find(new Long(client));
            MessageEntity m = new MessageEntity(message, subject, new Date(), conseiller.getMessageBox(), c.getMessageBox());
            daoMessage.create(m);
            return true;
        } else {
            return false;
        }
    }

    /**
     * From client to advisor
     *
     * @param subject
     * @param message
     * @return
     */
    @Override
    public boolean sendInternalEmail(String subject, String message) {
        ClientEntity c = clientService.getClient(clientService.getClientId());
        if(c.getConseiller() == null){
            return false;
        }
        ConseillerEntity con = c.getConseiller();
        MessageEntity m = new MessageEntity(message, subject, new Date(), c.getMessageBox(), c.getConseiller().getMessageBox());
        daoMessage.create(m);
        return true;
    }

    /**
     * Returns te number of pages for the current user
     *
     * @return
     */
    @Override
    public int getInternalEmailPageNum() {
        return 2;
    }

    @Override
    public List<Mail> getInternalEmailPage(int page, UserKind userKind) {
        List<Mail> retVal = new ArrayList<Mail>();
        
        if(userKind.equals(UserKind.MEMBER)){
            ClientEntity c = clientService.getClient(clientService.getClientId());
            List<MessageEntity> messages = c.getMessageBox().getMessagesRecu();
            for (MessageEntity message : messages) {
                Mail mail = new Mail();
                mail.setObject(message.getTitre());
                mail.setBody(message.getContenu());
                mail.setDate(message.getDateMessage().toString());
                retVal.add(mail);
            }
        }
        
        if(userKind.equals(UserKind.PRO)){
            ConseillerEntity c = conseillerService.getConseiller(conseillerService.getConseillerId());
            List<MessageEntity> messages = c.getMessageBox().getMessagesRecu();
            for (MessageEntity message : messages) {
                Mail mail = new Mail();
                mail.setObject(message.getTitre());
                mail.setBody(message.getContenu());
                mail.setDate(message.getDateMessage().toString());
                retVal.add(mail);
            }
        }

        return retVal;
    }
}
