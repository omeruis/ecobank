/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import models.NavigationContainer;
import org.springframework.stereotype.Service;
import service.NavigationService;

/**
 * Handle navigation
 * @author Lavenant
 */
@Service
public class NavigationServiceImpl implements NavigationService {
    private static int maxPageNum = 9;
    
    /**
     * Tool method, compute first and last page values
     * @param numberOfPages
     * @param page
     * @return
     * @throws NumberFormatException
     */
    public NavigationContainer ComputeNavigation(int numberOfPages, String page) throws NumberFormatException {
        NavigationContainer nav = new NavigationContainer();
        if (page == null) {
            nav.currentPage = 1;
        } else {
            nav.currentPage = Integer.parseInt(page);
        }
        if (numberOfPages <= maxPageNum) {
            nav.firstPage = 1;
            nav.lastPage = numberOfPages;
        } else {
            if (nav.currentPage - ((maxPageNum - 1) / 2) < 1) {
                nav.firstPage = 1;
                nav.lastPage = maxPageNum;
            } else if (nav.currentPage + ((maxPageNum - 1) / 2) > numberOfPages) {
                nav.lastPage = numberOfPages;
                nav.firstPage = numberOfPages - maxPageNum + 1;
            } else {
                nav.firstPage = nav.currentPage - ((maxPageNum - 1) / 2);
                nav.lastPage = nav.currentPage + ((maxPageNum - 1) / 2);
            }
        }
        return nav;
    }

    @Override
    public int GetMaxAccountDetailPerPage() {
        return 2;
    }

    @Override
    public int GetMaxMailPerPage() {
        return 2;
    }

    @Override
    public int GetClientPerPage() {
        return 2;
    }
}
