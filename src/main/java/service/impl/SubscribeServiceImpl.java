/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import models.Gender;
import models.MemberSubscribeForm;
import models.ProSubscribeForm;
import models.SubscribeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persistence.dao.ClientDAO;
import persistence.dao.ConseillerDAO;
import persistence.dao.MessageBoxDAO;
import persistence.entities.ClientEntity;
import persistence.entities.ConseillerEntity;
import persistence.entities.MessageBoxEntity;
import persistence.entities.ParticulierEntity;
import service.ClientService;
import service.ConseillerService;
import service.EmailService;
import service.SubscribeService;

/**
 * Handle all the subscription business logic
 * @author tlavenan
 */
@Service
public class SubscribeServiceImpl implements SubscribeService {

    @Autowired
    private EmailService emailService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ConseillerService conseillerService;

    @Resource
    ClientDAO dao;

    @Resource
    ConseillerDAO daoC;

    @Resource
    MessageBoxDAO daoBox;

    //MEMBER PART ==============================================================

    /**
     * Retrieve the form and creates a new member.
     * @param form
     * @return
     */
    @Override
    public boolean SubscribeMember(MemberSubscribeForm form) {
        if (form != null && form.getEmail() != null && form.getPasswd() != null) {
            ParticulierEntity c = new ParticulierEntity(form);
            Random r = new Random();
            Integer nb = 10000000 + r.nextInt(9999999);
            c.setNumero(nb.toString());
            c.setCode(form.getPasswd());
            if (dao.findByNumero(c.getNumero()) == null) {
                c.setCode(hashToSHA1(c.getCode()));
                MessageBoxEntity messageBox = new MessageBoxEntity();
                c.setMessageBox(messageBox);
                if (!daoC.findAll().isEmpty()) {
                    c.setConseiller(daoC.findAll().get(0));
                }
                dao.create(c);
                emailService.sendEmail(c.getEmail(), "Numero de compte Ecobank", "Votre numero de compte est : " + c.getNumero());
                return true;
            }
        }
        return false;
    }

    /**
     * Checks the form
     * @param form
     * @return
     */
    @Override
    public boolean MemberFormPasswordEquals(MemberSubscribeForm form) {
        return FormPasswordEquals(form);
    }

    /**
     * Checks the form
     * @param form
     * @return
     */
    @Override
    public boolean MemberFormWellFilled(MemberSubscribeForm form) {
        return FormWellFilled(form);
    }

    /**
     * Update the member information
     * @param formData
     * @return
     */
    @Override
    public boolean UpdateMember(MemberSubscribeForm formData) {
        return true;
    }

    /**
     * Prefill the form
     * @param attribute
     * @return
     */
    @Override
    public MemberSubscribeForm GetUserData(Object attribute) {
        MemberSubscribeForm retVal = new MemberSubscribeForm();
        ClientEntity c = clientService.getClient(clientService.getClientId());
        retVal.setAdress(c.getAdresse());
        retVal.setCity(c.getVille());
        retVal.setCountry(c.getPays());
        retVal.setEmail(c.getNumero());
        retVal.setFirst_name(c.getPrenom());
        retVal.setGender(c.getSexe());
        retVal.setLast_name(c.getNom());
        retVal.setPhone(c.getTelephone());
        retVal.setPostal_code(c.getCodePostale());

        return retVal;
    }

    //ADVISOR PART =============================================================

    /**
     * Retrieve the advisor subscription form and saves it
     * @param form
     * @return
     */
    @Override
    public boolean SubscribePro(ProSubscribeForm form) {
        if (form != null && form.getEmail() != null && form.getPasswd() != null) {
            ConseillerEntity c = new ConseillerEntity(form);
            c.setPassword(hashToSHA1(c.getPassword()));
            if (daoC.findByUserName(c.getUsername()) == null) {
                MessageBoxEntity messageBox = new MessageBoxEntity();
                c.setMessageBox(messageBox);
                daoC.create(c);
                return true;
            }
        }
        return false;
    }

    /**
     * Checks the form
     * @param form
     * @return
     */
    @Override
    public boolean ProFormPasswordEquals(ProSubscribeForm form) {
        return FormPasswordEquals(form);
    }

    /**
     * Checks the form
     * @param form
     * @return
     */
    @Override
    public boolean ProFormWellFilled(ProSubscribeForm form) {
        return FormWellFilled(form);
    }

    /**
     * Update advisor information
     * @param formData
     * @return
     */
    @Override
    public boolean UpdateAdvisor(ProSubscribeForm formData) {
        return true;
    }

    /**
     * Prefill the form
     * @param attribute
     * @return
     */
    @Override
    public ProSubscribeForm GetAdvisorData(Object attribute) {
        ProSubscribeForm retVal = new ProSubscribeForm();

        retVal.setAdress("10 route de Grenoble");
        retVal.setCity("La Fare-en-Champsaur");
        retVal.setCountry("France");
        retVal.setEmail("thibaut.lavenant@gmail.com");
        retVal.setFirst_name("Thibaut");
        retVal.setGender(Gender.FEMALE);
        retVal.setLast_name("Lavenant");
        retVal.setPhone("06 77 46 07 09");
        retVal.setPostal_code("05500");

        return retVal;
    }

    //GENERAL PART =============================================================
    
    private boolean FormPasswordEquals(SubscribeForm form) {
        if (form != null && form.getPasswdConfirm() != null && form.getPasswd() != null) {
            return form.getPasswd().equals(form.getPasswdConfirm());
        } else {
            return false;
        }
    }

    private boolean FormWellFilled(SubscribeForm form) {
        if (form != null && form.getAdress() != null
                && form.getCity() != null
                && form.getCountry() != null
                && form.getEmail() != null
                && form.getFirst_name() != null
                && form.getLast_name() != null
                && form.getPasswd() != null
                && form.getPasswdConfirm() != null
                && form.getPhone() != null
                && form.getPostal_code() != null) {
            return (form.getAdress().length() != 0
                    && form.getCity().length() != 0
                    && form.getCountry().length() != 0
                    && form.getEmail().length() != 0
                    && form.getFirst_name().length() != 0
                    && form.getLast_name().length() != 0
                    && form.getPasswd().length() != 0
                    && form.getPasswdConfirm().length() != 0
                    && form.getPhone().length() != 0
                    && form.getPostal_code().length() != 0);
        } else {
            return false;
        }
    }

    //==========================================================================
    //TOOLS
    public static String hashToSHA1(String str) {
        try {
            MessageDigest cript = MessageDigest.getInstance("SHA-1");
            cript.reset();
            cript.update(str.getBytes(Charset.forName("UTF8")));
            Formatter formatter = new Formatter();
            for (byte b : cript.digest()) {
                formatter.format("%02x", b);
            }
            String result = formatter.toString();
            formatter.close();
            return result;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ClientService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
