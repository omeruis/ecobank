/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utils;

import models.AccountCard;
import models.AccountHealth;
import models.AccountType;
import persistence.entities.CompteEntity;

/**
 *
 * @author omer
 */
public class AcountToCard {
    public static AccountCard ConvertToCard(CompteEntity c){
        AccountCard ac = new AccountCard();
        switch(c.getType()){
            case COMPTE_COURANT:
                ac.setType(AccountType.CURENT);
                break;
                
            case COMPTE_EPARGNE:
                ac.setType(AccountType.SAVE);
                break;
                
            case COMPTE_TITRE:
                ac.setType(AccountType.STOCK);
                break;
        }
        
        ac.setNumber(c.getNumero());
        ac.setAmount(c.getSolde());
        
        if(c.getSolde() > 1000){
            ac.setHealth(AccountHealth.GOOD);
        }else if(c.getSolde() > 500){
            ac.setHealth(AccountHealth.MEDIUM);
        }else{
            ac.setHealth(AccountHealth.BAD);
        }
        
        return ac;
    }
}
