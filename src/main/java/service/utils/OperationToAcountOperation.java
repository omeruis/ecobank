/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utils;

import models.AccountOperation;
import persistence.entities.OperationEntity;
import persistence.entities.VirementEntity;

/**
 *
 * @author omer
 */
public class OperationToAcountOperation {
    public static AccountOperation OperationConvert(OperationEntity o){
        AccountOperation ao = new AccountOperation();
        ao.setAmount(o.getMontant());
        ao.setDate(o.getDateOperation().toString());
        ao.setShortDesc(o.getLibelle());
        ao.setLongDesc(o.getDescription());
        return ao;
    }

    public static AccountOperation VirementConvert(VirementEntity o){
        AccountOperation ao = new AccountOperation();
        ao.setAmount(o.getMontant());
        ao.setDate(o.getDateOperation().toString());
        ao.setShortDesc(o.getLibelle());
        ao.setLongDesc(o.getDescription());
        return ao;
    }
}
