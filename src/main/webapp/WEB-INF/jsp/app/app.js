import * as angular from 'angular';
import 'angular-cookies';
import 'angular-ui-router';
// Import relatifs a l'application
import Home from "./home/Home";
import Router from "./router/Router";
import DirectivesCommunes from "./directives/DirectivesCommunes";

export default angular.module('ecobank', [
        'ui.router',
        'ngCookies',
        Home.name,
        Router.name,
        DirectivesCommunes.name
    ])

    .config([
        '$logProvider', '$compileProvider', function ($logProvider, $compileProvider) {
            console.info('Application built in debug mode.');
            //$compileProvider.commentDirectivesEnabled(false);
            //$compileProvider.cssClassDirectivesEnabled(false);
        }
    ])

    .run([
        '$rootScope', '$location', '$cookies', '$http', function ($rootScope, $location, $cookies, $http) {
            // keep user logged in after page refresh
            $rootScope.globals = $cookies.getObject('globals') || {};
            if ($rootScope.globals.currentUser) {
                $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
            }

            $rootScope.$on('$locationChangeStart', function (event, next, current) {
                // redirect to login page if not logged in and trying to access a restricted page
                var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/home']) === -1;
                var loggedIn = $rootScope.globals.currentUser;
                if (restrictedPage && !loggedIn) {
                    $location.path('/home');
                }
            });
        }
    ]);