export default angular.module('ecobank.App.directives', [])

/**
 * Directive utilisÃ©e uniquement pour passer le formulaire a invalid lorsque gel est sÃ©lÃ©ctionnÃ©
 */
/**
 * Permet d'ajouter la class clickable dÃ¨s qu'un element gÃ¨re un
 * ng-click la classe clickable se chargera elle de mettre un
 * curseur "pointer" pour que l'on sache que c'est "clickable"
 */
    .directive('ngClick', () => {
        return function (scope, element) {
            element.addClass('clickable');
        };
    })
    /**
     * Permet d'ajouter la class clickable dÃ¨s qu'un element gÃ¨re un
     * ui-sref la classe clickable se chargera elle de mettre un
     * curseur "pointer" pour que l'on sache que c'est "clickable"
     */
    .directive('uiSref', () => {
        return function (scope, element) {
            element.addClass('clickable');
        };
    });