import template from "./home.html";
import controller from "./Home.controller";

export default {
    name: 'home',
    component: {
        template,
        controller
    }
};
