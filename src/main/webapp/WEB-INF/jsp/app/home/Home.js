import angular from "angular";
import HomeComponent from "./Home.component";

export default angular.module('ecobank.home', [])
    .component(HomeComponent.name, HomeComponent.component)
;