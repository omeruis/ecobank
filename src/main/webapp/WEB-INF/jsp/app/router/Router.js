import HomeComponent from '../home/Home.component';

function Router($stateProvider, $urlRouterProvider) {
    const home = {
        abstract: false,
        name: 'home',
        state: {
            url: '/home',
            views: {
                'main@': {
                    component: HomeComponent.name
                }
            }
        }
    };

    $stateProvider
        .state(home.name, home.state);

    $urlRouterProvider.otherwise('/home');
}

Router.$inject = ['$stateProvider', '$urlRouterProvider'];

export default angular.module('ecobank.router', [])
    .config(Router);